package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.repositories.AuthoritiesRepository;
import com.gaming.project.gamingproject.repositories.FriendRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class AuthoritiesServiceImplTest {

    @InjectMocks
    AuthoritiesServiceImpl authoritiesService;
    @Mock
    AuthoritiesRepository authoritiesRepository;

    @Test
    public void doesAuthorityExist_should_check_ifExist() {

        Mockito.when(authoritiesService.doesAuthorityExist(Mockito.any())).thenReturn(true);

        authoritiesService.doesAuthorityExist("Michael");

        Mockito.verify(authoritiesRepository, Mockito.times(1)).existsById("Michael");
    }

    @Test
    public void deleteAuthority_should_deleteAuthority() {

        authoritiesService.deleteAuthority("Michael");

        Mockito.verify(authoritiesRepository, Mockito.times(1)).deleteById("Michael");
    }
}
