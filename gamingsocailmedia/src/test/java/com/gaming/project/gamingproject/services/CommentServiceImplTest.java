package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.entities.Comment;
import com.gaming.project.gamingproject.entities.Friend;
import com.gaming.project.gamingproject.entities.Request;
import com.gaming.project.gamingproject.exceptions.EntityNotFoundException;
import com.gaming.project.gamingproject.repositories.CommentReplyRepository;
import com.gaming.project.gamingproject.repositories.CommentRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class CommentServiceImplTest {

    private Comment comment = new Comment();

    @InjectMocks
    CommentServiceImpl commentService;
    @Mock
    CommentRepository commentRepository;

    @Test
    public void deleteCommentsByPostId_should_deleteFriendship() {
        commentService.deleteCommentsByPostId(1);

        Mockito.verify(commentRepository, Mockito.times(1)).deleteAllByPostId(1);
    }

    @Test
    public void createComment_Should_createComment() {
        commentService.createComment(new Comment());

        Mockito.verify(commentRepository, Mockito.times(1)).save(Mockito.any());
    }

    @Test
    public void deleteCommentById_should_deleteComment() {
        Mockito.when(commentRepository.findById(1)).thenReturn(java.util.Optional.ofNullable(comment));

        commentService.deleteCommentById(1);

        Mockito.verify(commentRepository, Mockito.times(1)).delete(comment);
    }
}
