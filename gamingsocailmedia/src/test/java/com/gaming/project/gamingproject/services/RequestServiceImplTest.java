package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.entities.Request;
import com.gaming.project.gamingproject.repositories.RequestRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class RequestServiceImplTest {

    @Mock
    RequestRepository requestRepository;
    @InjectMocks
    RequestServiceImpl requestService;


    @Test
    public void createRequest_Should_createRequest() {
        requestService.createRequest(new Request());

        Mockito.verify(requestRepository, Mockito.times(1)).save(Mockito.any());
    }

//    @Test
//    public void getAllUsersByEmail_Should_return_listOfUsers() {
//
//        List<RequestVisualisationModel> requests = new ArrayList<>();
//        Page<Request> pagedResponse = new PageImpl(requests);
//
//        Mockito.when(requestService.convertRequest(Mockito.any())).thenReturn((RequestVisualisationModel) pagedResponse);
//
//        requestService.getAllRequestsByUserName(principal,pageable);
//
//        Mockito.when(requestRepository.findAllByReceiverLoginUsername(Mockito.any(), Mockito.any())).thenReturn(pagedResponse);
//    }
}
