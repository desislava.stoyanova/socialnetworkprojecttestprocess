package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.entities.Comment;
import com.gaming.project.gamingproject.entities.Post;
import com.gaming.project.gamingproject.entities.PostAccessibility;
import com.gaming.project.gamingproject.entities.User;
import com.gaming.project.gamingproject.exceptions.EntityNotFoundException;
import com.gaming.project.gamingproject.models.CommentModel;
import com.gaming.project.gamingproject.models.PostModel;
import com.gaming.project.gamingproject.models.PostVisualisationModel;
import com.gaming.project.gamingproject.repositories.CommentRepository;
import com.gaming.project.gamingproject.repositories.LikeCommentRepository;
import com.gaming.project.gamingproject.repositories.PostRepository;
import com.gaming.project.gamingproject.repositories.UserRepository;
import com.gaming.project.gamingproject.services.contracts.CommentService;
import com.gaming.project.gamingproject.services.contracts.LikeCommentService;
import com.gaming.project.gamingproject.services.contracts.LikeService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class PostServiceImplTest {

    private CommentModel commentModel = new CommentModel();
    private PostModel postModel = new PostModel();
    private User loggedUser = new User();
    private Post post = new Post();

    private List<PostVisualisationModel> postVisualisationModelList = new ArrayList<>();

    private Page<PostVisualisationModel> page = new PageImpl<>(postVisualisationModelList);

    @Mock
    ModelMapper modelMapper;
    @Mock
    LikeCommentService likeCommentService;
    @Mock
    Principal principal;
    @Mock
    UserRepository userRepository;
    @Mock
    CommentRepository commentRepository;
    @Mock
    LikeCommentRepository likeCommentRepository;
    @Mock
    PostRepository postRepository;
    @InjectMocks
    PostServiceImpl postService;

    @Before
    public void setUp() {
        postVisualisationModelList.add(new PostVisualisationModel());
        postVisualisationModelList.add(new PostVisualisationModel());

        modelMapper = new ModelMapper();

        loggedUser.setPosts(new ArrayList<>());
        loggedUser.getPosts().add(new Post());
        loggedUser.setFirstName("Ivan");
        loggedUser.setLastName("Ivanov");
        loggedUser.setId(1);

        post.setId(1);
        post.setPostAccessibility(PostAccessibility.PUBLIC);

        postModel.setUserPicture("picture7");
        postModel.setUserId(1);
        postModel.setPostAccessibility("PUBLIC");
        postModel.setText("asdasd");

        commentModel.setPicture("picture7");
        commentModel.setPostId(1);
        commentModel.setText("asd");

    }

    @Test
    public void createPosts_Should_return_newPost() {
        postService.createPost(post);

        Mockito.verify(postRepository, Mockito.times(1)).save(Mockito.any());
    }

    @Test
    public void getById_Should_return_correctPost() {
        Mockito.when(postRepository.findById(1)).thenReturn(java.util.Optional.ofNullable(post));
        Post postResult = postService.getById(1);

        assertEquals(1, postResult.getId());
    }

    @Test
    public void deletePost_Should_remove_post() {
        postService.setCommentService(new CommentServiceImpl(commentRepository));
        postService.setLikeCommentService(new LikeCommentServiceImpl(likeCommentRepository));

        Mockito.when(postRepository.existsById(Mockito.any())).thenReturn(true);
        Mockito.when(postRepository.findById(1)).thenReturn(java.util.Optional.ofNullable(post));

        post.setComments(new ArrayList<>());
        post.getComments().add(new Comment());


        postService.deletePost(1);

        Mockito.verify(postRepository, Mockito.times(1)).delete(post);
    }

    @Test(expected = EntityNotFoundException.class)
    public void deletePost_Should_throw_exception_when_postIsMissing() {
        postService.setCommentService(new CommentServiceImpl(commentRepository));

        Mockito.when(postRepository.existsById(Mockito.any())).thenReturn(false);

        postService.deletePost(1);

        Mockito.verify(postRepository, Mockito.times(1)).delete(post);
    }

//    @Test
//    public void addCommentToPost_Should_addComment() {
//        postService.setUserService(new UserServiceImpl(userRepository));
//
//        Mockito.when(userRepository.findFirstByLoginUsername(Mockito.any())).thenReturn(loggedUser);
//        Mockito.when(postRepository.findById(1)).thenReturn(java.util.Optional.ofNullable(post));
//
//        postService.addCommentToPost(commentModel, principal);
//
//        Mockito.verify(postRepository, Mockito.times(1)).save(post);
//    }


//    @Test
//    public void addPost_Should_addPost() {
//        postService.setUserService(new UserServiceImpl(userRepository));
//
//        Mockito.when(userRepository.findFirstByLoginUsername(Mockito.any())).thenReturn(loggedUser);
//
//        postService.addPost(postModel, principal);
//
//        Mockito.verify(postRepository, Mockito.atLeastOnce()).save(post);
//    }
}
