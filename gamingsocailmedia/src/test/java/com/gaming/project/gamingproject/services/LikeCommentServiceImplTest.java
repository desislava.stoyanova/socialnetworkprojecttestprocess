package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.entities.Comment;
import com.gaming.project.gamingproject.entities.LikeComment;
import com.gaming.project.gamingproject.entities.Login;
import com.gaming.project.gamingproject.entities.User;
import com.gaming.project.gamingproject.exceptions.EntityNotFoundException;
import com.gaming.project.gamingproject.models.LikeCommentModel;
import com.gaming.project.gamingproject.repositories.CommentRepository;
import com.gaming.project.gamingproject.repositories.LikeCommentRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.security.Principal;

@RunWith(MockitoJUnitRunner.class)
public class LikeCommentServiceImplTest {

    private LikeCommentModel likeCommentModel = new LikeCommentModel();
    private LikeComment likeComment = new LikeComment();
    private User user1 = new User();
    private Comment comment1 = new Comment();

    @InjectMocks
    LikeCommentServiceImpl likeCommentService;
    @Mock
    LikeCommentRepository likeCommentRepository;
    @Mock
    UserServiceImpl userService;
    @Mock
    CommentRepository commentRepository;
    @Mock
    Principal principal;

    @Before
    public void setUp() {

        likeCommentModel.setCommentId(1);
        likeCommentModel.setUserId(1);
        user1.setId(1);
        user1.setLogin(new Login("Ivan", "asd", true));
        comment1.setId(1);
        likeComment.setId(1);

    }

    @Test
    public void existsByPostIdAndUserId_Should_returnInformationAboutExisting() {
        Mockito.when(likeCommentService.existsByCommentIdAndUserId(1,1)).thenReturn(true);

        likeCommentService.existsByCommentIdAndUserId(1,1);

        Mockito.verify(likeCommentRepository, Mockito.times(1)).existsByCommentIdAndUserId(1,1);
    }

    @Test
    public void createLikeComment_Should_return_likeToComment() {
        likeCommentService.createLikeComment(likeComment);

        Mockito.verify(likeCommentRepository, Mockito.times(1)).save(Mockito.any());
    }

    @Test
    public void getLike_by_PostIdAndUserId_should_return_Like() {
        Mockito.when(likeCommentRepository.findByUserIdAndCommentId(1,1)).thenReturn(likeComment);

        likeCommentService.getByUserIdAndCommentId(1,1);

        Mockito.verify(likeCommentRepository, Mockito.times(1)).findByUserIdAndCommentId(1, 1);
    }

    @Test
    public void addLikeToComment_should_return_LikeToComment() {
        likeCommentService.setCommentService(new CommentServiceImpl(commentRepository));

        Mockito.when(commentRepository.findById(1)).thenReturn(java.util.Optional.of(comment1));
        Mockito.when(principal.getName()).thenReturn("Ivan");
        Mockito.when(userService.getUser("Ivan")).thenReturn(user1);

        likeCommentService.addLikeToComment(likeCommentModel,principal);

        Mockito.verify(commentRepository, Mockito.times(1)).save(comment1);
    }

    @Test(expected = EntityNotFoundException.class)
    public void addLikeToComment_should_throw_exception_when_LikeDoesNotExist() {
        likeCommentService.setCommentService(new CommentServiceImpl(commentRepository));

        Mockito.when(commentRepository.findById(1)).thenReturn(java.util.Optional.of(comment1));
        Mockito.when(principal.getName()).thenReturn("Ivan");
        Mockito.when(userService.getUser("Ivan")).thenReturn(user1);
        Mockito.when(likeCommentService.existsByCommentIdAndUserId(1,1)).thenReturn(true);

        likeCommentService.addLikeToComment(likeCommentModel,principal);

        Mockito.verify(commentRepository, Mockito.times(1)).save(comment1);
    }

    @Test
    public void addDislikeToComment_should_return_removeLikeToComment() {
        likeCommentService.setCommentService(new CommentServiceImpl(commentRepository));

        Mockito.when(commentRepository.findById(1)).thenReturn(java.util.Optional.of(comment1));
        Mockito.when(principal.getName()).thenReturn("Ivan");
        Mockito.when(userService.getUser("Ivan")).thenReturn(user1);
        Mockito.when(likeCommentService.getByUserIdAndCommentId(1,1)).thenReturn(likeComment);
        Mockito.when(likeCommentService.existsByCommentIdAndUserId(1,1)).thenReturn(true);

        likeCommentService.dislike(likeCommentModel,principal);

        Mockito.verify(commentRepository, Mockito.times(1)).save(comment1);
    }

    @Test(expected = EntityNotFoundException.class)
    public void addDislikeToComment_should_throw_exception_when_LikeDoesNotExist() {
        likeCommentService.setCommentService(new CommentServiceImpl(commentRepository));

        Mockito.when(commentRepository.findById(1)).thenReturn(java.util.Optional.of(comment1));
        Mockito.when(principal.getName()).thenReturn("Ivan");
        Mockito.when(userService.getUser("Ivan")).thenReturn(user1);
        Mockito.when(likeCommentService.existsByCommentIdAndUserId(1,1)).thenReturn(false);

        likeCommentService.dislike(likeCommentModel,principal);

        Mockito.verify(commentRepository, Mockito.times(1)).save(comment1);
    }

}
