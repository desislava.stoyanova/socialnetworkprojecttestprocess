package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.entities.ConfirmationToken;
import com.gaming.project.gamingproject.exceptions.EntityNotFoundException;
import com.gaming.project.gamingproject.repositories.ConfirmationTokenRepository;
import com.gaming.project.gamingproject.services.contracts.ConfirmationTokenService;
import jdk.nashorn.internal.parser.Token;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ConfirmationTokenServiceImplTest {

    @InjectMocks
    private ConfirmationTokenServiceImpl confirmationTokenService;

    @Mock
    private ConfirmationTokenRepository tokenRepository;

    @Mock
    private ConfirmationTokenServiceImpl serviceImplTest;

    private ConfirmationToken confirmationToken = new ConfirmationToken();

    @Before
    public void setUp() {
        confirmationToken.setId(1);
        confirmationToken.setConfirmationToken("testToken");
    }

    @Test
    public void findByConfirmationToken_Should_GetTokenByTokenContent() {
        when(tokenRepository.findByConfirmationToken("testToken")).thenReturn(confirmationToken);
        when(tokenRepository.existsByConfirmationToken("testToken")).thenReturn(true);

        confirmationTokenService.findByConfirmationToken("testToken");

        verify(tokenRepository, Mockito.times(1)).findByConfirmationToken("testToken");
    }

    @Test(expected = EntityNotFoundException.class)
    public void findByConfirmationToken_Should_throw_exception_whenEntityNotFound() {
        when(tokenRepository.existsByConfirmationToken("testToken")).thenReturn(false);

        confirmationTokenService.findByConfirmationToken("testToken");

        verify(tokenRepository, Mockito.times(1)).findByConfirmationToken("testToken");
    }

    @Test
    public void createToken_Should_addNewToken() {
        confirmationTokenService.createToken(new ConfirmationToken());

        Mockito.verify(tokenRepository, Mockito.times(1)).save(Mockito.any());
    }
}
