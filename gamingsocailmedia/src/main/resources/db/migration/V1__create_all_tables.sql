create table users
(
    username varchar(50) not null
        primary key,
    password varchar(68) not null,
    enabled  tinyint     not null
);

create table authorities
(
    username  varchar(50) not null,
    authority varchar(50) not null,
    constraint username_authority
        unique (username, authority),
    constraint FK__users
        foreign key (username) references users (username)
);

create table users_no
(
    id            int auto_increment
        primary key,
    first_name    varchar(55)                      null,
    last_name     varchar(55)                      null,
    email         varchar(55)                      null,
    age           int                              null,
    gender        enum ('MALE', 'FEMALE', 'OTHER') null,
    country       varchar(55)                      null,
    description   varchar(255)                     null,
    login_id      varchar(55)                      null,
    register_time timestamp                        null,
    picture       varchar(200)                     null,
    constraint users_no_users_username_fk
        foreign key (login_id) references users (username)
);

create table confirmation_tokens
(
    id                 bigint auto_increment
        primary key,
    confirmation_token varchar(255) null,
    created_time       datetime     null,
    user_id            int          not null,
    constraint confirmation_tokens_users_no_id_fk
        foreign key (user_id) references users_no (id)
);

create table friend_request
(
    id          int auto_increment
        primary key,
    sender_id   int null,
    receiver_id int null,
    constraint friend_request_users_id_fk
        foreign key (sender_id) references users_no (id),
    constraint friend_request_users_id_fk_2
        foreign key (receiver_id) references users_no (id)
);

create table friends
(
    id          int auto_increment
        primary key,
    sender_id   int null,
    receiver_id int null,
    constraint friends_users_no_id_fk
        foreign key (sender_id) references users_no (id),
    constraint friends_users_no_id_fk_2
        foreign key (receiver_id) references users_no (id)
);

create table posts
(
    id                 int auto_increment
        primary key,
    user_id            int                        null,
    text               varchar(255)               null,
    likes              int default 0              null,
    post_accessibility enum ('PUBLIC', 'PRIVATE') null,
    creation_time      timestamp                  null,
    updated_time       timestamp                  null,
    video_id           varchar(255)               null,
    photo              varchar(255)               null,
    constraint posts_users_id_fk
        foreign key (user_id) references users_no (id)
);

create table comments
(
    id            int auto_increment
        primary key,
    content       varchar(255) null,
    likes         int          null,
    post_id       int          null,
    user_id       int          null,
    comment_id    int          null,
    creation_time timestamp    null,
    constraint comments_comments_id_fk
        foreign key (comment_id) references comments (id),
    constraint comments_posts_id_fk
        foreign key (post_id) references posts (id),
    constraint comments_users_no_id_fk
        foreign key (user_id) references users_no (id)
);

create table comments_replies
(
    id         int         not null
        primary key,
    content    varchar(25) null,
    likes      int         null,
    comment_id int         null,
    user_id    int         null,
    constraint comments_replies_comments_id_fk
        foreign key (comment_id) references comments (id),
    constraint comments_replies_users_no_id_fk
        foreign key (user_id) references users_no (id)
);

create table likes
(
    id      int auto_increment
        primary key,
    post_id int null,
    user_id int null,
    constraint likes_post_id_fk
        foreign key (post_id) references posts (id),
    constraint likes_user_no_id_fk
        foreign key (user_id) references users_no (id)
);

create table likes_comment
(
    id         int auto_increment
        primary key,
    comment_id int           null,
    user_id    int           null,
    likes      int default 0 null,
    constraint likes_comment_id_fk
        foreign key (comment_id) references comments (id)
);

