"use strict";
$(function () {
    sendRequest();
    getUserToCheckRequest();
    removeFromFriends();
});

function removeFromFriends() {
    let $userId = $(".user-profile").attr("id");
    $(".buttons-content").on("click", ".remove-friendship", function () {
        fetch("http://localhost:8080/friends/" + $userId, {
            method: "DELETE",
            headers: {
                "Content-type":"application/json",
                "Accept":"application/json, text/plain, */*"
            }
        })
            .then(function () {
                let $buttonPlace = $(".buttons-content");
                $buttonPlace.empty();
                $buttonPlace.append(
                    "<button class=\"btn btn-primary followbtn send-friendship\">Add Friend</button>"
                );
                alert("Successfully made operation");
            })
            .catch((err) => {
                console.log(err.message);
            })
    })
}

function getUserToCheckRequest() {
    let $userId = $(".user-profile").attr("id");
    console.log($userId);
    fetch("http://localhost:8080/users/check/" + $userId, {
        method: "GET",
        headers: {
            "Content-type":"application/json",
            "Accept":"application/json, text/plain, */*"
        }
    })
        .then((res) => res.json())
        .then(function (el) {
            let $buttonPlace = $(".buttons-content");
            $buttonPlace.empty();
            console.log("success");
            console.log(el);
            if (el.requestType === "ACCEPTED") {
                $buttonPlace.append(
                    // "<button disabled class=\"btn btn-success followbtn accepted-friendship\">Accepted</button>" +
                    "<button class=\"btn btn-danger cd-popup-trigger followbtn remove-friendship\">Remove</button>"
                );
            } else if (el.requestType === "PENDING") {
                $buttonPlace.append(
                    "<button disabled class=\"btn btn-warning followbtn pending-friendship\">Pending</button>"
                );
            } else {
                $buttonPlace.append(
                    "<button class=\"btn btn-primary followbtn send-friendship\">Add Friend</button>"
                );
            }
        })
        .catch((err) => {
            console.log(err.message);
        })
}

function sendRequest() {
    $(".buttons-content").on("click", ".send-friendship", function () {
        let $userId = $(".user-profile").attr("id");
        console.log($userId);
        fetch("http://localhost:8080/request/" + $userId, {
            method: "PUT",
            headers: {
                "Content-type":"application/json",
                "Accept":"application/json, text/plain, */*"
            }
        })
            .then(function () {
                let $buttonPlace = $(".buttons-content");
                $buttonPlace.empty();
                $buttonPlace.append(
                    "<button disabled class=\"btn btn-warning followbtn pending-friendship\">Pending</button>"
                );
            })
            .catch((err) => {
                console.log(err.message);
            })
    })
}