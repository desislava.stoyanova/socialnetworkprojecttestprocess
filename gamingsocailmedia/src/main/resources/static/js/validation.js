"use strict";
$(function () {
    validateUsername();
    validatePassword();
    validateFirstName();
    validateLastName();
    validateAge();
});

function validateUsername() {
    $("#username-reg").keyup(function () {
        let $value = this.value;
        let $errorField = $("#username-error");
        let $submitButton = $("#register-button");
        if ($value.length === 0) {
            $errorField.empty();
            $errorField.append("Username can't be empty").css("color", "red").css("font-size", "14px");
            $submitButton.attr("disabled", true);
            return false;
        } else if ($value.length <= 4 || $value.length >= 24) {
            $errorField.empty();
            $errorField.append("Username should be between 4 and 24 symbols").css("color", "red").css("font-size", "14px");
            $submitButton.attr("disabled", true);
            return false;
        } else {
            $errorField.empty();
            $submitButton.attr("disabled", false);
            return true;
        }
    })
}

function validatePassword() {
    $("#password-reg").keyup(function () {
        let $value = this.value;
        let $errorField = $("#password-error");
        let $submitButton = $("#register-button");
        if ($value.length === 0) {
            $errorField.empty();
            $errorField.append("Password can't be empty").css("color", "red").css("font-size", "14px");
            $submitButton.attr("disabled", true);
        } else if ($value.length <= 5 || $value.length >= 20) {
            $errorField.empty();
            $errorField.append("Password should be between 6 and 20 symbols").css("color", "red").css("font-size", "14px");
            $submitButton.attr("disabled", true);
        } else {
            $errorField.empty();
            $submitButton.attr("disabled", false);
        }
    })
}

function validateFirstName() {
    $("#first-name-reg").keyup(function () {
        let $value = this.value;
        let $errorField = $("#first-name-error");
        let $submitButton = $("#register-button");
        if ($value.length === 0) {
            $errorField.empty();
            $errorField.append("First Name can't be empty").css("color", "red").css("font-size", "14px");
            $submitButton.attr("disabled", true);
        } else if ($value.length <= 2 || $value.length >= 24) {
            $errorField.empty();
            $errorField.append("First Name should be between 3 and 24 symbols").css("color", "red").css("font-size", "14px");
            $submitButton.attr("disabled", true);
        } else {
            $errorField.empty();
            $submitButton.attr("disabled", false);
        }
    })
}

function validateLastName() {
    $("#last-name-reg").keyup(function () {
        let $value = this.value;
        let $errorField = $("#last-name-error");
        let $submitButton = $("#register-button");
        if ($value.length === 0) {
            $errorField.empty();
            $errorField.append("Last Name can't be empty").css("color", "red").css("font-size", "14px");
            $submitButton.attr("disabled", true);
        } else if ($value.length <= 2 || $value.length >= 24) {
            $errorField.empty();
            $errorField.append("Last Name should be between 3 and 24 symbols").css("color", "red").css("font-size", "14px");
            $submitButton.attr("disabled", true);
        } else {
            $errorField.empty();
            $submitButton.attr("disabled", false);
        }
    })
}

function validateAge() {
    $("#age-reg").keyup(function () {
        let $value = this.value;
        let $errorField = $("#age-error");
        let $submitButton = $("#register-button");
        if ($value.match(/^[0-9]+$/) === null) {
            $errorField.empty();
            $errorField.append("Enter digit").css("color", "red").css("font-size", "14px");
            $submitButton.attr("disabled", true);
            return false;
        }
        $value = parseInt($value);
        if ($value.length === 0) {
            $errorField.empty();
            $errorField.append("Age can't be empty").css("color", "red").css("font-size", "14px");
            $submitButton.attr("disabled", true);
        } else if ($value === 0) {
            $errorField.empty();
            $errorField.append("Age can't be zero").css("color", "red").css("font-size", "14px");
            $submitButton.attr("disabled", true);
        }
        else if ($value <= 12 || $value >= 120) {
            $errorField.empty();
            $errorField.append("Age should be between 12 and 120").css("color", "red").css("font-size", "14px");
            $submitButton.attr("disabled", true);
        } else {
            $errorField.empty();
            $submitButton.attr("disabled", false);
        }
    })
}