$(document).ready(function () {
    addPost2();
});


function addPost2() {
    $("#submit-post").on("click", function () {
        let $postContent = $("#post-content").val();
        let $postVisibility = $("#post-visibility").val();
        let obj = {
            "text": $postContent,
            "postAccessibility": $postVisibility
        };

        fetch("http://localhost:8080/posts/", {
            method: "POST",
            body: JSON.stringify(obj),
            headers: {
                "Content-type": "application/json",
                "Accept": "application/json, text/plain, */*"
            }
        })
            .then((res) => res.json())
            .then(function (el) {
                console.log(el);
                $("#post-conteiner").prepend(
                    "<div id='post-" + el.id + "' class=\"panel panel-default\">\n" +
                    "                <div class=\"col-md-12\">\n" +
                    "                    <div class=\"media\">\n" +
                    "                        <div class=\"media-left\"><a href=\"javascript:void(0)\"> <img\n" +
                    "                                 src= 'images/" + el.userPicture + "' alt=\"\" class=\"media-object\">\n" +
                    "                        </a></div>\n" +
                    "                        <div class=\"media-body\">\n" +
                    "                            <br>\n" +
                    "                            <h4 class=\"media-heading\"><span>" + el.userFirstName + " " + el.userLastName + " </span> <br>\n" +
                    "                                <small><i class=\"fa fa-clock-o\"></i>  " + el.creationTime + "</small></h4>\n" +
                    "                            <p><span>" + el.text + "</span></p>\n" +
                    "\n" +
                    "                            <ul class=\"nav nav-pills pull-left \">\n" +
                    "                                <li><a href=\"\" title=\"\"><i class=\"glyphicon glyphicon-thumbs-up\"></i> " +
                    "<span> " + el.totalLikes + " </span> </a></li>\n" +
                    "                                <li><a href=\"\" title=\"\"><i class=\" glyphicon glyphicon-comment\"></i> 25</a></li>\n" +
                    "                                <li><a href=\"\" title=\"\"><i class=\"glyphicon glyphicon-share-alt\"></i> 15 </a></li>" +
                    "                            </ul>\n" +
                    "<br>\n" +
                    "<br>\n" +
                    "<div class=\"comment-content-test\"></div>" +
                    "<div class=\"add-comment-section\"></div>" +
                    "                        </div>\n" +
                    "                    </div>\n" +
                    "                </div>\n" +
                    "                </div>\n"
                );

                $("#post-conteiner").find("#post-" + el.id + " .add-comment-section").append(
                    "<div class=\"col-md-12 border-top comment-replay\">\n" +
                    "                                <div class=\"status-upload\">\n" +
                    "                                        <label>Comment</label>\n" +
                    "                                                                                                                      \n" +
                    "                                        <textarea class=\"comment-content form-control\" placeholder=\"Comment here\"></textarea>\n" +
                    "                                        <br>\n" +
                    "                                        <ul class=\"nav nav-pills pull-left \">\n" +
                    "                                            <li><a title=\"\"><i class=\"glyphicon glyphicon-bullhorn\"></i></a></li>\n" +
                    "                                            <li><a title=\"\"><i class=\" glyphicon glyphicon-facetime-video\"></i></a></li>\n" +
                    "                                            <li><a title=\"\"><i class=\"glyphicon glyphicon-picture\"></i></a></li>\n" +
                    "                                        </ul>\n" +
                    "                                        <button id=\"submit-comment\" type=\"submit\" class=\"btn btn-success pull-right submit-comment testSubmit\"> Comment</button>\n" +
                    "                                        <div class=\"pull-right btn btn-primary button-like btn-primary\"><span class=\"glyphicon glyphicon-thumbs-up\"></span> Like</div>" +
                    "                                      \n" +
                    "                                </div>\n" +
                    "                            </div>"
                );

                $("#post-content").val("");
            })
            .catch((err) => {
                console.log(err.message);
            })
    })
}