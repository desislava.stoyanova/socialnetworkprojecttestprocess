"use strict";
$(function () {
    fillEditPopup();
    addUserPosts(3,0);
    nextPagePosts();
    previousPagePosts();
    clearPostsCommentContent();
    addUserComments();
});

function clearPostsCommentContent() {
    $("#close-pop").on("click", function () {
        $(".user-posts-comments").empty();
    })
}

let userIdValue = undefined;

function fillEditPopup() {
    let testVar = undefined;
    let $newOne = undefined;
    $("#table-content").on("click", ".edit-user-final" , function () {
        let userId = $(this).closest(".some-content-here").find(".users-list-name").attr("id");
        userIdValue = userId;
        // addUserComments(userId, 3, 0);
        $newOne = $(this).closest(".some-content-here").find(".users-list-name");
        testVar = userId;
         fetch("http://localhost:8080/users/" + userId, {
             method: "GET",
             headers: {
                 "Content-type":"application/json",
                 "Accept":"application/json, text/plain, */*"
             }
         })
             .then((res) => res.json())
             .then(function (ele) {
                 console.log(ele);
                 $("#first-name-edit").val(ele.firstName);
                 $("#last-name-edit").val(ele.lastName);
                 $("#email-edit").val(ele.email);
                 $("#age-edit").val(ele.age);
                 $("#gender-edit").val(ele.gender);
                 $("#country-edit").val(ele.country);
             })
             .catch((err) => {
                 console.log(err.message);
             })
    });

    $(".submit-edit").on("click", function () {
        let obj = {
            "id": testVar,
            "firstName": $("#first-name-edit").val(),
            "lastName": $("#last-name-edit").val(),
            "email": $("#email-edit").val(),
            "age": $("#age-edit").val(),
            "gender": $("#gender-edit").val(),
            "country": $("#country-edit").val()
        };

        fetch("http://localhost:8080/users/", {
            method: "PUT",
            body: JSON.stringify(obj),
            headers: {
                "Content-type":"application/json",
                "Accept":"application/json, text/plain, */*"
            }
        })
            .then((res) => res.json())
            .then(function (el) {
                console.log(el);
                $($newOne).text(el.firstName);
            })
            .catch((err) => {
                console.log(err.message);
            })
    })
}

function addUserPosts(size, page) {
    $(".all-posts").on("click", function () {
        $("#next-page-edit").removeClass("comment-edit-button");
        $("#current-page-value-edit").removeClass("comment-edit-button");
        $("#previous-page-edit").removeClass("comment-edit-button");

        $("#next-page-edit").addClass("").addClass("post-edit-button");
        $("#current-page-value-edit").text(1).addClass("post-edit-button");
        $("#previous-page-edit").addClass("").addClass("post-edit-button");
        $.ajax({
            url: window.location.origin + "/posts/page/" + userIdValue,
            type: "GET",
            data: {
                size: size,
                page: page
            },
            success: function (response) {
                let $postContent = $(".user-posts-comments");
                $postContent.empty();

                response.content.forEach(function (el) {
                    $postContent.append(
                        "<div id='post-edit-" + el.id + "' class=\"col-12 mb-2 border border-success rounded post-content\">\n" +
                        "                            <div>" + el.userFirstName + " " + el.userLastName + "</div>\n" +
                        "<div class='edit-delete-buttons float-right'>" +
                        "<button type=\"button\" class=\"btn btn-primary btn-rounded btn-sm my-0 edit-post\">Edit</button>" +
                        "<button type=\"button\" class=\"btn btn-danger btn-rounded btn-sm my-0 delete-post\">Remove</button>" +
                        "</div>" +
                        "                            <div>" + el.creationTime + "</div>\n" +
                        "<textarea id=\"post-content\" class=\"form-control\"\n" +
                        "                                      placeholder=\"Edit Post Here\">" + el.text + "</textarea>" +
                        "                            <span>Likes: <input class='col-xs-2 likes-value-edit' type='text' value='" + el.totalLikes + "'></span>\n" +
                        "                        </div>"
                    )
                });

                $("#current-page-value-edit").text(response.pageable.pageNumber + 1);
                enableDisableButton($("#previous-page-edit"), response.first);
                enableDisableButton($("#next-page-edit"), response.last);
            },
            error: function (err) {
                console.log(err);
                console.log("error");
            }
        })
    })
}

function nextPageRequest(type, value) {
    $.ajax({
        url: window.location.origin + "/" + type + "/page/" + userIdValue,
        type: "GET",
        data: {
            size: 3,
            page: value
        },
        success: function (response) {
            pageResponseBody(response);
        },
        error: function (err) {
            console.log(err);
            console.log("error");
        }
    })
}

function pageResponseBody(response) {
    let $postContent = $(".user-posts-comments");
    $postContent.empty();
    response.content.forEach(function (el) {
        $postContent.append(
            "<div id='post-edit-" + el.id + "' class=\"col-12 mb-2 border border-success rounded\">\n" +
            "                            <div>" + el.userFirstName + " " + el.userLastName + "</div>\n" +
            "<div class='edit-delete-buttons float-right'>" +
            "<button type=\"button\" class=\"btn btn-primary btn-rounded btn-sm my-0\">Edit</button>" +
            "<button type=\"button\" class=\"btn btn-danger btn-rounded btn-sm my-0\">Remove</button>" +
            "</div>" +
            "                            <div>" + el.creationTime + "</div>\n" +
            "<textarea id=\"post-content\" class=\"form-control\"\n" +
            "                                      placeholder=\"Edit Post Here\">" + el.text + "</textarea>" +
            "                            <span>Likes: <input class='col-xs-2' type='text' value='" + el.totalLikes + "'></span>\n" +
            "                        </div>"
        )
    });

    $("#current-page-value-edit").text(response.pageable.pageNumber + 1);
    enableDisableButton($("#previous-page-edit"), response.first);
    enableDisableButton($("#next-page-edit"), response.last);
}

function previousPageRequest(type, value) {
    $.ajax({
        url: window.location.origin + "/" + type + "/page/" + userIdValue,
        type: "GET",
        data: {
            size: 3,
            page: value
        },
        success: function (response) {
            pageResponseBody(response);
        },
        error: function (err) {
            console.log(err);
            console.log("error");
        }
    })
}

function nextPagePosts() {
    $("#next-page-edit").on("click", function () {
        let value = $("#current-page-value").text();
        console.log(userIdValue);
        console.log(value);
        if ($("#previous-page-edit").hasClass("post-edit-button")) {
            previousPageRequest("posts", value);
        } else if ($("#previous-page-edit").hasClass("comment-edit-button")) {
            previousPageRequest("comments", value);
        }
    })
}

function previousPagePosts() {
    $("#previous-page-edit").on("click", function () {
        let value = parseInt($("#current-page-value").text()) - 2;
        console.log(userIdValue);
        console.log(value);
        if ($("#next-page-edit").hasClass("post-edit-button")) {
            nextPageRequest("posts", value);
        } else if ($("#next-page-edit").hasClass("comment-edit-button")) {
            nextPageRequest("comments", value);
        }
    })
}

function addUserComments() {
    $(".all-comments").on("click", function () {
        $("#next-page-edit").removeClass("post-edit-button");
        $("#current-page-value-edit").removeClass("post-edit-button");
        $("#previous-page-edit").removeClass("post-edit-button");

        $("#next-page-edit").addClass("comment-edit-button");
        $("#current-page-value-edit").text(1).addClass("comment-edit-button");
        $("#previous-page-edit").addClass("comment-edit-button");
        $.ajax({
            url: window.location.origin + "/comments/page/" + userIdValue,
            type: "GET",
            data: {
                size: 3,
                page: 0
            },
            success: function (response) {
                let $postContent = $(".user-posts-comments");
                $postContent.empty();

                response.content.forEach(function (el) {
                    $postContent.append(
                        "<div id='post-edit-" + el.id + "' class=\"col-12 mb-2 border border-success rounded comment-content\">\n" +
                        "                            <div>" + el.userFirstName + " " + el.userLastName + "</div>\n" +
                        "<div class='edit-delete-buttons float-right'>" +
                        "<button type=\"button\" class=\"btn btn-primary btn-rounded btn-sm my-0 edit-comment\">Edit</button>" +
                        "<button type=\"button\" class=\"btn btn-danger btn-rounded btn-sm my-0 delete-comment\">Remove</button>" +
                        "</div>" +
                        "                            <div>" + el.creationTime + "</div>\n" +
                        "<textarea id=\"comment-content\" class=\"form-control\"\n" +
                        "                                      placeholder=\"Edit Post Here\">" + el.text + "</textarea>" +
                        "                            <span>Likes: <input class='col-xs-2 likes-value-edit' type='text' value='" + el.totalLikes + "'></span>\n" +
                        "                        </div>"
                    )
                });

                $("#current-page-value-edit").text(response.pageable.pageNumber + 1);
                enableDisableButton($("#previous-page-edit"), response.first);
                enableDisableButton($("#next-page-edit"), response.last);
            },
            error: function (err) {
                console.log(err);
                console.log("error");
            }
        })
    })
}