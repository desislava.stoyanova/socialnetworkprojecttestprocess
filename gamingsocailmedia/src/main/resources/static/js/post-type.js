"use strict";
$(function () {
    changeType();
    changeTypeText();
});

function changeType() {
    $("#video-upload").on("click", function () {
        let $postTypeContent = $("#post-type-content");
        $postTypeContent.empty();
        $postTypeContent.append(
            "<div class='pt-2 pb-2'>" +
            "<span>Add Youtube link here</span>" +
            "<div class=\"input-group mb-3\">\n" +
            "  <input id='video-upload-input' type=\"text\" class=\"form-control\" aria-label=\"Default\" aria-describedby=\"inputGroup-sizing-default\">\n" +
            "</div>" +
            "</div>"
        );
    })
}

function changeTypeText() {
    $("#text-upload").on("click", function () {
        let $postTypeContent = $("#post-type-content");
        $postTypeContent.empty();
    })
}