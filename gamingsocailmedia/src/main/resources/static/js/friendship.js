"use strict";
$(function () {
    sendFriendRequest();
    acceptFriendRequest();
    declineRequest();
});

function sendFriendRequest() {
    $("#table-content").on("click", ".add-friend-button", function () {
        let requestButton = this;
        let $userId = $(this).closest(".some-content-here").attr("id");
        let idNumber = $userId.substr(5);

        fetch("http://localhost:8080/request/" + idNumber, {
            method: "PUT",
            headers: {
                "Content-type":"application/json",
                "Accept":"application/json, text/plain, */*"
            }
        })
            .then(function (response) {
               $(requestButton).hide();
            })
            .catch((err) => {
                console.log(err.message);
            })
    })
}

function acceptFriendRequest() {
    $("#request-content").on("click", ".accept-request", function () {
        let $content = $(this).closest(".some-content-here");
        let $userId = $(this).closest(".some-content-here").attr("id");
        let idNumber = $userId.substr($userId.lastIndexOf("-") + 1);

        console.log($userId);
        console.log(idNumber);
        fetch("http://localhost:8080/friends/" + idNumber, {
            method: "PUT",
            headers: {
                "Content-type":"application/json",
                "Accept":"application/json, text/plain, */*"
            }
        })
            .then(function (response) {
                $content.empty();
            })
            .catch((err) => {
                console.log(err.message);
            })
    } )
}

function declineRequest() {
    $("#request-content").on("click", ".decline-request", function () {
        let $content = $(this).closest(".some-content-here");
        let $requestId = $(this).closest(".some-content-here").attr("id");
        console.log($requestId);
        let firstSymbolIdPosition = $requestId.indexOf("-") + 1;
        let secondSymbolIdPosition = $requestId.lastIndexOf("-") + 1;
        console.log(firstSymbolIdPosition);
        console.log(secondSymbolIdPosition);
        let firstUserId = $requestId.substring(firstSymbolIdPosition, secondSymbolIdPosition - 1);
        console.log(firstUserId);
        let secondUserId = $requestId.substr(secondSymbolIdPosition);
        console.log(secondUserId);

        fetch("http://localhost:8080/request/" + firstUserId + "-" + secondUserId, {
            method: "DELETE",
            headers: {
                "Content-type":"application/json",
                "Accept":"application/json, text/plain, */*"
            }
        })
            .then(function () {
                $content.empty();
                 $("#table-content").find("#user-" + secondUserId + " .button-section").prepend(
                        "<a href=\"#\" class=\"btn btn-primary add-friend-button btn-primary\"><span class=\"glyphicon glyphicon-send\"></span> Add</a>"
                    );
            })
            .catch((err) => {
                console.log(err.message);
            })
    })
}