"use strict";
$(function () {
    getAndLoadContent(8, 0);
    nextPage();
    previousPage();
});

function previousPage() {
    $("#previous-page").on("click", function () {
        getAndLoadContent(8, parseInt($("#current-page-value").text()) - 2);
    })
}

function nextPage() {
    $("#next-page").on("click", function () {
        getAndLoadContent(8, parseInt($("#current-page-value").text()));
    })
}

function getAndLoadContent(size, page) {
    $.ajax({
        url: window.location.origin + "/users/page/",
        type: "GET",
        data: {
            size: size,
            page: page,
        },
        success: function (response) {
            let $tableContent = $("#table-content");
            $tableContent.empty();
            response.content.forEach(function (el) {
                $tableContent.append(
                    "<div class='col-3 some-content-here user-content'>" +
                    "<figure class='rectangle-form'>" +
                    "   <img src= 'images/" + el.picture + "' alt=\"\">" +
                    "<figcaption cla>" +
                    "   <div id='"+ el.id +"' style='font-size: smaller' class='users-list-name'>"+ el.firstName + " " + el.lastName +"</div>" +
                    "   <div class='row'>" +
                    "       <a class=\"button edit-user-final btn btn-primary\" href=\"#popup1\">Edit</a>" +
                    "       <div class='col-6'>" +
                    "           <button type='button' class='btn btn-primary btn-xs delete-user'>Del</button>" +
                    "       </div>" +
                    "   </div>" +
                    "</figcaption>" +
                    "</figure>" +
                    "</div>"
                )
            });
            $("#current-page-value").text(response.pageable.pageNumber + 1);
            enableDisableButton($("#previous-page"), response.first);
            enableDisableButton($("#next-page"), response.last);
        },
        error: function (err) {
            console.log(err);
            console.log("error");
        }
    })
}

function enableDisableButton($button, shouldDisabled) {
    if (shouldDisabled) {
        $button.addClass("li-disabled");
    } else if ($button.hasClass("li-disabled")) {
        $button.removeClass("li-disabled");
    }
}