"use strict";
$(function () {
    searchUserByFirstNameOrLastName();
    deletePost();
    editPost();
    deleteComment();
    editComment();
    deleteUser();
});

function deleteUser() {
    $("#table-content").on("click", ".delete-user", function () {
        let $userBox = $(this).closest(".user-content");
        let $userIdDiv = $(this).closest(".user-content").find(".users-list-name");
        let userId = $userIdDiv.attr("id");

        $.ajax({
            url: window.location.origin + "/users/" + userId,
            type: "DELETE",
            success: function () {
                // console.log("success")
                $userBox.remove();
            },
            error: function (err) {
                console.log(err);
                console.log("error");
            }
        })
    })
}

function searchUserByFirstNameOrLastName() {
    $("#search-user-field").keyup(function () {
        let searchBoxValue = this.value;
        if (searchBoxValue.length === 0) {
            getAndLoadContent(8, 0);
        } else {
            let url = new URL("http://localhost:8080/users/filter");
            let params = {firstName: searchBoxValue, lastName: searchBoxValue};
            url.search = new URLSearchParams(params).toString();

            fetch(url, {
                method: "GET",
                headers: {
                    "Content-type":"application/json",
                    "Accept":"application/json, text/plain, */*"
                }
            })
                .then((res) => res.json())
                .then(function (el) {
                    if (el.length === 0) {
                        $("#table-content").empty();
                    } else {
                        let $tableContent = $("#table-content");
                        $tableContent.empty();

                        el.forEach(function (ele) {
                            $tableContent.append(
                                "<div class='col-3 some-content-here'>" +
                                "<figure class='rectangle-form'>" +
                                "   <img class='user-pic' src='http://icons.iconarchive.com/icons/papirus-team/papirus-status/96/avatar-default-icon.png' alt='User Image'>" +
                                "<figcaption cla>" +
                                "   <div id='"+ ele.id +"' style='font-size: smaller' class='users-list-name'>"+ ele.firstName + " " + ele.lastName +"</div>" +
                                "   <div class='row'>" +
                                "       <a class=\"button edit-user-final btn btn-primary\" href=\"#popup1\">Edit</a>" +
                                "       <div class='col-6'>" +
                                "           <button type='button' class='btn btn-primary btn-xs'>Del</button>" +
                                "       </div>" +
                                "   </div>" +
                                "</figcaption>" +
                                "</figure>" +
                                "</div>"
                            )
                        });
                    }
                })
                .catch((err) => {
                    console.log(err.message);
                });
        }
    })
}

function deletePost() {
    $(".user-posts-comments").on("click", ".delete-post", function () {
        let $postContent = $(this).closest(".post-content");
        let $postId = $postContent.attr("id");
        let lastDelimiter = $postId.lastIndexOf("-");
        $postId = $postId.substr(lastDelimiter + 1);
        $.ajax({
            url: window.location.origin + "/posts/" + $postId,
            type: "DELETE",
            success: function () {
                $postContent.remove();
            },
            error: function (err) {
                console.log(err);
                console.log("error");
            }
        })
    })
}

function editPost() {
    $(".user-posts-comments").on("click", ".edit-post", function () {
        let $postContent = $(this).closest(".post-content");
        let $postId = $postContent.attr("id");
        let lastDelimiter = $postId.lastIndexOf("-");
        $postId = $postId.substr(lastDelimiter + 1);
        let $postText = $(this).closest(".post-content").find("#post-content");
        let $totalLikes = $(this).closest(".post-content").find(".likes-value-edit");

        let obj = {
            "id": parseInt($postId),
            "text": $postText.val(),
            "totalLikes": parseInt($totalLikes.val())
        };

        fetch(window.location.origin + "/posts/", {
            method: "PUT",
            body: JSON.stringify(obj),
            headers: {
                "Content-type":"application/json",
                "Accept":"application/json, text/plain, */*"
            }
        })
            .then((res) => res.json())
            .then(function (el) {
                $postText.empty();
                $totalLikes.empty();
                $postText.val(el.text);
                $totalLikes.val(el.totalLikes);
            })
            .catch(err => {
                console.log(err);
            });

    })
}

function deleteComment() {
    $(".user-posts-comments").on("click", ".delete-comment", function () {
        let $postContent = $(this).closest(".comment-content");
        let $postId = $postContent.attr("id");
        let lastDelimiter = $postId.lastIndexOf("-");
        $postId = $postId.substr(lastDelimiter + 1);
        $.ajax({
            url: window.location.origin + "/comments/" + $postId,
            type: "DELETE",
            success: function () {
                $postContent.remove();
            },
            error: function (err) {
                console.log(err);
                console.log("error");
            }
        })
    })
}

function editComment() {
    $(".user-posts-comments").on("click", ".edit-comment", function () {
        let $commentContent = $(this).closest(".comment-content");
        let $commentId = $commentContent.attr("id");
        let lastDelimiter = $commentId.lastIndexOf("-");
        $commentId = $commentId.substr(lastDelimiter + 1);
        let $commentText = $(this).closest(".comment-content").find("#comment-content");
        let $totalLikes = $(this).closest(".comment-content").find(".likes-value-edit");

        console.log($commentId);
        console.log($commentText.val());
        console.log($totalLikes.val());

        let obj = {
            "id": parseInt($commentId),
            "text": $commentText.val(),
            "totalLikes": parseInt($totalLikes.val())
        };

        fetch(window.location.origin + "/comments/", {
            method: "PUT",
            body: JSON.stringify(obj),
            headers: {
                "Content-type":"application/json",
                "Accept":"application/json, text/plain, */*"
            }
        })
            .then((res) => res.json())
            .then(function (el) {
                $commentText.empty();
                $totalLikes.empty();
                $commentText.val(el.text);
                $totalLikes.val(el.totalLikes);
            })
            .catch(err => {
                console.log(err);
            });
    })
}