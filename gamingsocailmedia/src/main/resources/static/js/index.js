$(function () {
    fillEditPopup();
});

function scrollFooter(scrollY, heightFooter)
{
    console.log(scrollY);
    console.log(heightFooter);

    if(scrollY >= heightFooter)
    {
        $('footer').css({
            'bottom' : '0px'
        });
    }
    else
    {
        $('footer').css({
            'bottom' : '-' + heightFooter + 'px'
        });
    }
}

$(window).load(function(){
    var windowHeight        = $(window).height(),
        footerHeight        = $('footer').height(),
        heightDocument      = (windowHeight) + ($('.content').height()) + ($('footer').height()) - 20;

    // Definindo o tamanho do elemento pra animar
    $('#scroll-animate, #scroll-animate-main').css({
        'height' :  heightDocument + 'px'
    });

    // Definindo o tamanho dos elementos header e conteúdo
    $('header').css({
        'height' : windowHeight + 'px',
        'line-height' : windowHeight + 'px'
    });

    $('.wrapper-parallax').css({
        'margin-top' : windowHeight + 'px'
    });

    scrollFooter(window.scrollY, footerHeight);

    // ao dar rolagem
    window.onscroll = function(){
        var scroll = window.scrollY;

        $('#scroll-animate-main').css({
            'top' : '-' + scroll + 'px'
        });

        $('header').css({
            'background-position-y' : 50 - (scroll * 100 / heightDocument) + '%'
        });

        scrollFooter(scroll, footerHeight);
    }
});






/* ===== Logic for creating fake Select Boxes ===== */
$('.sel').each(function() {
    $(this).children('select').css('display', 'none');

    var $current = $(this);

    $(this).find('option').each(function(i) {
        if (i == 0) {
            $current.prepend($('<div>', {
                class: $current.attr('class').replace(/sel/g, 'sel__box')
            }));

            var placeholder = $(this).text();
            $current.prepend($('<span>', {
                class: $current.attr('class').replace(/sel/g, 'sel__placeholder'),
                text: placeholder,
                'data-placeholder': placeholder
            }));

            return;
        }

        $current.children('div').append($('<span>', {
            class: $current.attr('class').replace(/sel/g, 'sel__box__options'),
            text: $(this).text()
        }));
    });
});

// Toggling the `.active` state on the `.sel`.
$('.sel').click(function() {
    $(this).toggleClass('active');
});

// Toggling the `.selected` state on the options.
$('.sel__box__options').click(function() {
    var txt = $(this).text();
    var index = $(this).index();

    $(this).siblings('.sel__box__options').removeClass('selected');
    $(this).addClass('selected');

    var $currentSel = $(this).closest('.sel');
    $currentSel.children('.sel__placeholder').text(txt);
    $currentSel.children('select').prop('selectedIndex', index + 1);
});


function fillEditPopup() {

    let testVar = undefined;
    let $newOne = undefined;

    $("#table-content").on("click", ".edit-user-final", function () {
        let userId = $(this).closest("#table-content").find(".user-id-current").attr("id");
        $newOne = $(this).closest(".some-content-here").find(".users-list-name");
        testVar = userId;

        $.ajax({
            url: window.location.origin + "/posts/page/public/",
            type: "GET",
            data: {
                size: 4,
                page: 0,
            },
            success: function (response) {
                console.log("TOP:::");
                console.log(response);
                let $topPostsContent = $(".form-group");

                response.content.forEach(function (el) {
                    $topPostsContent.append(
                        "<div class=\"panel12 panel-default1\">\n" +
                        "                <div class=\"col-md-12\">\n" +
                        "                    <div class=\"media1\">\n" +
                        "                        <div class=\"media-left\"><img\n" +
                        "                                src= 'images/" + el.userPicture + "' alt=\"\" class=\"media-object1\">\n" +
                        "                        </a></div>\n" +
                        "                        <div class=\"media-body1\">\n" +
                        "                            <br>\n" +
                        "<span class=\"pull-right font-weight-bold1 \">LIKES: " + el.totalLikes + "</span>" +
                        "                            <h4 class=\"media-heading1\"><span>" + el.userFirstName + " " + el.userLastName + " </span> <br>\n" +
                        "                                <small><i class=\"fa fa-clock-o\"></i>  " + el.creationTime + "</small></h4>\n" +
                        "                            <p><span>" + el.text + "</span></p>\n" +
                        "                        </div>\n" +
                        "                    </div>\n" +
                        "                </div>\n" +
                        "                </div>\n"
                    );
                })

            },
            error: function (err) {
                console.log(err);
                console.log("error");
            }
        })
    })
}