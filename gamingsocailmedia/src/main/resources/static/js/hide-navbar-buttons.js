"use strict";
$(function () {
    hideAdminButton();
});

function hideAdminButton() {
    if ($("#user-role").text()  === "USER") {
        $("#admin-redirect").hide();
    }
}
