package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.entities.Friend;
import com.gaming.project.gamingproject.entities.User;
import com.gaming.project.gamingproject.models.FriendVisualisationModel;
import com.gaming.project.gamingproject.repositories.FriendRepository;
import com.gaming.project.gamingproject.services.contracts.FriendService;
import lombok.Setter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class FriendServiceImpl implements FriendService {

    private FriendRepository friendRepository;
    private ModelMapper mapper;

    @Autowired
    public FriendServiceImpl(FriendRepository friendRepository, ModelMapper mapper) {
        this.friendRepository = friendRepository;
        this.mapper = mapper;
    }

    @Override
    public void createFriendship(Friend friend) {
        friendRepository.save(friend);
    }

    @Override
    public boolean doesFriendshipExist(User sender, User receiver) {
        return friendRepository.existsBySenderIdAndReceiverId(sender.getId(), receiver.getId());
    }

    @Transactional
    @Override
    public void deleteFriendShip(User sender, User receiver) {
        friendRepository.deleteBySenderAndReceiver(sender, receiver);
    }

    @Override
    public List<Friend> findFriendsByUserName(String userFirstName) {
        return friendRepository.findAllByReceiver_Login_Username(userFirstName);
    }

    @Override
    public List<FriendVisualisationModel> convertListFriend(List<Friend> friends) {
        return friends.stream()
                .map(this::convertFriend)
                .collect(Collectors.toList());
    }

    private FriendVisualisationModel convertFriend(Friend friend) {
        return mapper.map(friend, FriendVisualisationModel.class);
    }
}
