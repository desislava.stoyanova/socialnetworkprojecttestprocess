package com.gaming.project.gamingproject.configuration;

import com.gaming.project.gamingproject.entities.RoleType;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Role;
import org.springframework.http.HttpMethod;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final DataSource dataSource;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(dataSource);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf()
                .disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.DELETE, "/posts/**")
                .hasAnyRole(RoleType.ADMIN.toString(), RoleType.USER.toString())
                .antMatchers(HttpMethod.PUT, "/users/**")
                .hasAnyRole(RoleType.USER.toString(), RoleType.ADMIN.toString())
                .antMatchers(HttpMethod.POST, "/posts/")
                .hasAnyRole(RoleType.USER.toString(), RoleType.ADMIN.toString())
                .antMatchers(HttpMethod.POST, "/users/addComment")
                .hasAnyRole(RoleType.USER.toString(), RoleType.ADMIN.toString())
                .antMatchers(HttpMethod.GET, "/users/posts-{username}", "/users/filter",
                        "/users/filter/email")
                .hasAnyRole(RoleType.USER.toString(), RoleType.ADMIN.toString())
                .antMatchers(HttpMethod.GET, "/posts/")
                .permitAll()
                .antMatchers("/home", "/friends")
                .hasAnyRole( RoleType.USER.toString(), RoleType.ADMIN.toString())
                .antMatchers("/", "/users", "/register-success", "/account-verified")
                .permitAll()
                .antMatchers("/admin")
                .hasRole(RoleType.ADMIN.toString())
                .antMatchers("/comment")
                .hasAnyRole( RoleType.USER.toString(), RoleType.ADMIN.toString())
                .and()
                .formLogin()
                .loginPage("/login")
                .loginProcessingUrl("/authenticate")
                .defaultSuccessUrl("/home", true)
                .permitAll()
                .and()
                .logout()
                .permitAll()
                .and()
                .exceptionHandling()
                .accessDeniedPage("/access-denied");

        http.authorizeRequests()
                .antMatchers("/resources/**")
                .permitAll()
                .anyRequest()
                .permitAll();
    }

    @Bean
    public UserDetailsManager userDetailsManager() {
        JdbcUserDetailsManager jdbcUserDetailsManager = new JdbcUserDetailsManager();
        jdbcUserDetailsManager.setDataSource(dataSource);
        return jdbcUserDetailsManager;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}