package com.gaming.project.gamingproject.services.contracts;

import com.gaming.project.gamingproject.entities.ConfirmationToken;
import com.gaming.project.gamingproject.entities.GenderType;
import com.gaming.project.gamingproject.entities.User;
import com.gaming.project.gamingproject.models.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;

import java.security.Principal;
import java.util.List;

public interface UserService {

    void addUser(User user);

    void createUser(RegisterUserModel registerUserModel);

    void sendRequest(int currUser, Principal principal);

    void addAsFriend(int currUser, Principal principal);

    void removeFromFriends(int currUser, Principal principal);

    void removeFriendRequest(int currUser, int wantedUser);

    List<User> findAllByGenderType(GenderType genderType);

    Page<UserModel> userPagination(Pageable pageable, Principal principal);

    UserModel findByIdModel(int id);

    UserModel editUser(UserModel userModel);

    User getUser(String userName);

    User findById(int id);

    Long countUsers();

    List<UserModel> getAllUsersByFirstNameOrLastName(String name, String lastName);

    UserRequestCheckModel findUserById(int id, Principal principal);

    void deleteUserById(Integer userId);

    User findByEmailIgnoreCase(String email);

    ConfirmationToken createUserAndToken(RegisterUserModel user);

    String getUserRole(Authentication principal);
}