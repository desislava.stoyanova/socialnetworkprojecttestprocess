package com.gaming.project.gamingproject.repositories;

import com.gaming.project.gamingproject.entities.Like;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LikeRepository extends CrudRepository<Like, Integer> {

    boolean existsByPostIdAndUserId(int postId, int userId);

    Like findByUserIdAndPostId(int userId, int postId);
}
