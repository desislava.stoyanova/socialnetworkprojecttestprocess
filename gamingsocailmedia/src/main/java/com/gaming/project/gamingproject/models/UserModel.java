package com.gaming.project.gamingproject.models;

import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.util.List;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserModel {

    @NotNull
    @PositiveOrZero
    private Integer id;

    @Size(min = 2, max = 24)
    private String firstName;

    @Size(min = 2, max = 24)
    private String lastName;

    @Size(min = 5, max = 34)
    private String email;

    private Integer age;

    @NotNull
    private String gender;

    @NotNull
    private String country;

    @Size(min = 5, max = 100)
    private String description;

    private String loginUsername;

    private String picture;

    private boolean hasFriend;

    private List<PostVisualisationModel> posts;

    private List<CommentVisualizationModel> comments;
}
