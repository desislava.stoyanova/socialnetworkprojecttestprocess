package com.gaming.project.gamingproject.controllers.rest;

import com.gaming.project.gamingproject.models.RequestVisualisationModel;
import com.gaming.project.gamingproject.services.contracts.RequestService;
import com.gaming.project.gamingproject.services.contracts.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.security.Principal;

@RestController
@RequestMapping("/request")
@RequiredArgsConstructor
public class RequestRestController {

    private final UserService userService;
    private final RequestService requestService;

    @PutMapping("/{currUser}")
    public void addFriend(@PositiveOrZero @NotNull @PathVariable Integer currUser, Principal principal) {
        userService.sendRequest(currUser, principal);
    }

    @DeleteMapping("/{currUser}-{wantedUser}")
    public void removeFriendRequest(@PositiveOrZero @NotNull @PathVariable Integer currUser, @PathVariable @PositiveOrZero @NotNull Integer wantedUser) {
        userService.removeFriendRequest(currUser, wantedUser);
    }

    @GetMapping("/page/")
    public Page<RequestVisualisationModel> getPostsByPage(Principal principal, Pageable pageable) {
        return requestService.getAllRequestsByUserName(principal, pageable);
    }
}




