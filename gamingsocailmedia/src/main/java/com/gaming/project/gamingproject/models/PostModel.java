package com.gaming.project.gamingproject.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PostModel {

    @NotNull
    private int userId;

    @Size(min = 1, max = 255)
    private String text;

    @NotNull
    private String postAccessibility;

    private String userPicture;

    private String videoId;

    private String photo;

}
