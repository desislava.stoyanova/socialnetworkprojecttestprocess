package com.gaming.project.gamingproject.repositories;

import com.gaming.project.gamingproject.entities.Authorities;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthoritiesRepository extends CrudRepository<Authorities, String> {
}
