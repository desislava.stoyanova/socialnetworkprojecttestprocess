package com.gaming.project.gamingproject.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PostEditModel {

    @PositiveOrZero
    @NotNull
    private Integer id;

    @NotNull
    private String text;

    @PositiveOrZero
    private Integer totalLikes;
}
