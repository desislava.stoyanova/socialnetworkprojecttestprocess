package com.gaming.project.gamingproject.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "friend_request")
public class Request {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private User receiver;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private User sender;

    public Request(User receiver, User sender) {
        this.receiver = receiver;
        this.sender = sender;
    }
}
