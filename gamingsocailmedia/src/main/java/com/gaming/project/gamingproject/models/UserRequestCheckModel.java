package com.gaming.project.gamingproject.models;

import com.gaming.project.gamingproject.entities.RequestType;
import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserRequestCheckModel {

    private RequestType requestType;
}
