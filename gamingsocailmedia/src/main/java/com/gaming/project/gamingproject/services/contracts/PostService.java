package com.gaming.project.gamingproject.services.contracts;

import com.gaming.project.gamingproject.entities.Post;
import com.gaming.project.gamingproject.models.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.security.Principal;
import java.util.List;

public interface PostService {

    Post createPost(Post post);

    Post getById(int id);

    void deletePost(Integer postId);

    PostEditModel editPost(PostEditModel postId);

    Page<PostVisualisationModel> findTop5PostsByLikes(Pageable pageable, Principal principal);

    Page<PostVisualisationModel> getPostForUserPage(Pageable pageable, Principal principal);

    Page<PostVisualisationModel> findPostsByUsername(Pageable pageable, String userLoginUsername, Principal principal);

    Page<PostVisualisationModel> postsPagination(Pageable pageable, Principal principal);

    PostVisualisationModel addPost(PostModel postModel, Principal principal);

    Page<PostVisualisationModel> findPostsForUser(Pageable pageable, Integer id);

    CommentVisualizationModel addCommentToPost(CommentModel commentModel, Principal principal);

    Page<PostVisualisationModel> findAllPublicPosts(Pageable pageable);
}
