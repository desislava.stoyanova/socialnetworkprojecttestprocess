package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.entities.Comment;
import com.gaming.project.gamingproject.entities.LikeComment;
import com.gaming.project.gamingproject.entities.User;
import com.gaming.project.gamingproject.exceptions.EntityNotFoundException;
import com.gaming.project.gamingproject.models.LikeCommentModel;
import com.gaming.project.gamingproject.repositories.LikeCommentRepository;
import com.gaming.project.gamingproject.services.contracts.CommentService;
import com.gaming.project.gamingproject.services.contracts.LikeCommentService;
import com.gaming.project.gamingproject.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;

@Service
public class LikeCommentServiceImpl implements LikeCommentService {

    private LikeCommentRepository likeCommentRepository;
    private CommentService commentService;
    private UserService userService;

    @Autowired
    public LikeCommentServiceImpl(LikeCommentRepository likeCommentRepository,
                                  CommentService commentService, UserService userService) {
        this.likeCommentRepository = likeCommentRepository;
        this.commentService = commentService;
        this.userService = userService;
    }

    public LikeCommentServiceImpl(LikeCommentRepository likeCommentRepository) {
        this.likeCommentRepository = likeCommentRepository;
    }

    @Override
    public boolean existsByCommentIdAndUserId(int commentId, int userId) {
        return likeCommentRepository.existsByCommentIdAndUserId(commentId, userId);
    }

    @Override
    public void createLikeComment(LikeComment likeComment) {
        likeCommentRepository.save(likeComment);
    }

    @Override
    public LikeComment getByUserIdAndCommentId(int userId, int commentId) {
        return likeCommentRepository.findByUserIdAndCommentId(userId,commentId);
    }

    @Override
    public Integer addLikeToComment(LikeCommentModel likeCommentModel, Principal principal) {
        Comment comment = commentService.getById(likeCommentModel.getCommentId());
        User user = userService.getUser(principal.getName());

        if (existsByCommentIdAndUserId(comment.getId(), user.getId())) {
            throw new EntityNotFoundException("Not correct exception");
        }

        LikeComment likeComment = new LikeComment(comment, user);

        createLikeComment(likeComment);
        comment.setTotalLikes(comment.getTotalLikes() + 1);
        commentService.createComment(comment);
        return comment.getTotalLikes();
    }

    @Override
    public Integer dislike(LikeCommentModel likeCommentModel, Principal principal) {
        Comment comment = commentService.getById(likeCommentModel.getCommentId());
        User user = userService.getUser(principal.getName());

        if (!existsByCommentIdAndUserId(comment.getId(), user.getId())) {
            throw new EntityNotFoundException("Not correct exception");
        }
        LikeComment likeComment = getByUserIdAndCommentId(user.getId(), likeCommentModel.getCommentId());
        likeCommentRepository.delete(likeComment);
        comment.setTotalLikes(comment.getTotalLikes() - 1);
        commentService.createComment(comment);
        return comment.getTotalLikes();
    }

    @Transactional
    @Override
    public void deleteCommentLikesByCommentId(Integer commentId) {
        likeCommentRepository.deleteAllByCommentId(commentId);
    }

    @Transactional
    @Override
    public void deleteCommentLikesByUserId(Integer userId) {
        likeCommentRepository.deleteAllByUserId(userId);
    }

    public void setCommentService(CommentService commentService) {
        this.commentService = commentService;
    }
}
