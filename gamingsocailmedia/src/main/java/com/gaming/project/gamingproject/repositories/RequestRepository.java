package com.gaming.project.gamingproject.repositories;

import com.gaming.project.gamingproject.entities.Request;
import com.gaming.project.gamingproject.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RequestRepository extends CrudRepository<Request, Integer> {

    boolean existsBySenderIdAndReceiverId(int senderId, int receiverId);

    void deleteBySenderAndReceiver(User sender, User receiver);

    Page<Request> findAllByReceiverLoginUsername(String receiverLoginUsername, Pageable pageable);
}
