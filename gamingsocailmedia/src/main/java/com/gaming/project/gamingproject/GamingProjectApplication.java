package com.gaming.project.gamingproject;

import com.gaming.project.gamingproject.controllers.FileUploadController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import java.io.File;

@SpringBootApplication
@ComponentScan({"com.gaming.project.gamingproject","com.gaming.project.gamingproject.controllers"})
public class GamingProjectApplication {

    public static void main(String[] args) {
        new File(FileUploadController.uploadDirectory).mkdir();
        SpringApplication.run(GamingProjectApplication.class, args);
    }
}