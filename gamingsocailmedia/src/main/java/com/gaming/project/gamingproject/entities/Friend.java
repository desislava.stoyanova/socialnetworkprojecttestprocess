package com.gaming.project.gamingproject.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "friends")
public class Friend {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private User receiver;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private User sender;

    public Friend(User receiver, User sender) {
        this.receiver = receiver;
        this.sender = sender;
    }
}
