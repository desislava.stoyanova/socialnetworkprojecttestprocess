package com.gaming.project.gamingproject.services.contracts;

import com.gaming.project.gamingproject.entities.LikeComment;
import com.gaming.project.gamingproject.models.LikeCommentModel;

import java.security.Principal;

public interface LikeCommentService {

    boolean existsByCommentIdAndUserId(int commentId, int userId);

    void createLikeComment(LikeComment likeComment);

    LikeComment getByUserIdAndCommentId(int userId, int commentId);

    Integer addLikeToComment(LikeCommentModel likeCommentModel, Principal principal);

    Integer dislike(LikeCommentModel likeCommentModel, Principal principal);

    void deleteCommentLikesByUserId(Integer userId);

    void deleteCommentLikesByCommentId(Integer commentId);
}