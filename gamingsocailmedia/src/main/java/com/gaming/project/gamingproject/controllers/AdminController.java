package com.gaming.project.gamingproject.controllers;

import com.gaming.project.gamingproject.entities.GenderType;
import com.gaming.project.gamingproject.entities.User;
import com.gaming.project.gamingproject.entities.UserCountries;
import com.gaming.project.gamingproject.repositories.PostRepository;
import com.gaming.project.gamingproject.repositories.UserRepository;
import com.gaming.project.gamingproject.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

@Controller
public class AdminController {

    private final UserService userService;

    @Autowired
    public AdminController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/admin")
    public String adminPage(Model model, Principal principal) {
        User user = userService.getUser(principal.getName());
        model.addAttribute("user", user);
        model.addAttribute("allUsers", userService.countUsers());
        model.addAttribute("males", userService.findAllByGenderType(GenderType.MALE).size());
        model.addAttribute("females", userService.findAllByGenderType(GenderType.FEMALE).size());
        model.addAttribute("others", userService.findAllByGenderType(GenderType.OTHER).size());
        model.addAttribute("countries", UserCountries.values());
        model.addAttribute("genders", GenderType.values());

        return "admin";
    }
}
