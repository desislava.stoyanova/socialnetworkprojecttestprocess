package com.gaming.project.gamingproject.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class EntityAlreadyExist extends RuntimeException {

    public EntityAlreadyExist() { }

    public EntityAlreadyExist(String message) { super(message); }

}
