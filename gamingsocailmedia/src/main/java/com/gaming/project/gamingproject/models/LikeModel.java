package com.gaming.project.gamingproject.models;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

@Getter
@Setter
public class LikeModel {

    @NotNull
    @PositiveOrZero
    private int postId;

    @NotNull
    @PositiveOrZero
    private int userId;
}
