package com.gaming.project.gamingproject.controllers;

import com.gaming.project.gamingproject.entities.GenderType;
import com.gaming.project.gamingproject.entities.UserCountries;
import com.gaming.project.gamingproject.models.LoginModel;
import com.gaming.project.gamingproject.models.RegisterUserModel;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

@Controller
public class LoginController {

    @GetMapping("/login")
    public String showLogin(Model model) {
        model.addAttribute("login", new LoginModel());
        model.addAttribute("user", new RegisterUserModel());
        model.addAttribute("countries", UserCountries.values());
        model.addAttribute("genders", GenderType.values());
        return "index";
    }

    @GetMapping("/access-denied")
    public String showAccessDenied(Model model, Authentication principal) {
        model.addAttribute("isUser", principal.isAuthenticated());
        return "access-denied";
    }
}