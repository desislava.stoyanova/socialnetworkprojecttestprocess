package com.gaming.project.gamingproject.controllers.rest;

import com.gaming.project.gamingproject.models.FriendVisualisationModel;
import com.gaming.project.gamingproject.services.contracts.FriendService;
import com.gaming.project.gamingproject.services.contracts.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/friends")
@RequiredArgsConstructor
public class FriendRestController {

    private final UserService userService;
    private final FriendService friendService;

    @GetMapping("/{username}")
    public List<FriendVisualisationModel> getFriends(@Size(min = 2, max = 24) @NotNull @PathVariable String username) {
        return friendService.convertListFriend(friendService.findFriendsByUserName(username));
    }

    @PutMapping("/{currUser}")
    public void addInFriendList(@PositiveOrZero @NotNull @PathVariable Integer currUser, Principal principal) {
        userService.addAsFriend(currUser, principal);
    }

    @DeleteMapping("/{currUser}")
    public void removeFriend(@PositiveOrZero @NotNull @PathVariable Integer currUser, Principal principal) {
        userService.removeFromFriends(currUser, principal);
    }
}
