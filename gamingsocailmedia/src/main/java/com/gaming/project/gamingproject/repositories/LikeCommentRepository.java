package com.gaming.project.gamingproject.repositories;

import com.gaming.project.gamingproject.entities.LikeComment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LikeCommentRepository  extends CrudRepository<LikeComment, Integer> {

    boolean existsByCommentIdAndUserId(int commentId, int userId);

    LikeComment findByUserIdAndCommentId(int userId, int commentId);

    void deleteAllByUserId(Integer userId);

    void deleteAllByCommentId(Integer commentId);
}
