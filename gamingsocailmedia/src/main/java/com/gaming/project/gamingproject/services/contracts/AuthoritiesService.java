package com.gaming.project.gamingproject.services.contracts;

public interface AuthoritiesService {

    boolean doesAuthorityExist(String username);

    void deleteAuthority(String username);
}
