package com.gaming.project.gamingproject.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CommentEditModel {

    @NotNull
    private Integer id;

    @Size(min = 1, max = 255)
    @NotNull
    private String text;

    @PositiveOrZero
    private Integer totalLikes;
}
