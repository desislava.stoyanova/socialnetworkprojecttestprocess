package com.gaming.project.gamingproject.controllers;

import com.gaming.project.gamingproject.entities.RoleType;
import com.gaming.project.gamingproject.entities.User;
import com.gaming.project.gamingproject.services.contracts.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

@Controller
@RequiredArgsConstructor
public class UserHomeController {

    private final UserService userService;

    @GetMapping("/home")
    public String userPostPage(Model model, Authentication principal) {
        User user = userService.getUser(principal.getName());
        model.addAttribute("user", user);
        model.addAttribute("userRole", userService.getUserRole(principal));
        return "post-page";
    }
}
