package com.gaming.project.gamingproject.services.contracts;

import com.gaming.project.gamingproject.entities.Comment;
import com.gaming.project.gamingproject.models.CommentEditModel;
import com.gaming.project.gamingproject.models.CommentVisualizationModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CommentService {

    Comment createComment(Comment comment);

    Comment getById(int id);

    CommentVisualizationModel convertComment(Comment comment);

    void deleteCommentsByPostId(int postId);

    Page<CommentVisualizationModel> findCommentsForUser(Pageable pageable, Integer userId);

    void deleteCommentById(Integer commentId);

    CommentEditModel editComment(CommentEditModel commentModel);
}
