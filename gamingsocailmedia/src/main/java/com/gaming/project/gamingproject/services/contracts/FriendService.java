package com.gaming.project.gamingproject.services.contracts;

import com.gaming.project.gamingproject.entities.Friend;
import com.gaming.project.gamingproject.entities.User;
import com.gaming.project.gamingproject.models.FriendVisualisationModel;

import java.util.List;

public interface FriendService {

    void createFriendship(Friend friend);

    boolean doesFriendshipExist(User sender, User receiver);

    void deleteFriendShip(User sender, User receiver);

    List<FriendVisualisationModel> convertListFriend(List<Friend> friends);

    List<Friend> findFriendsByUserName (String userLoginUsername);
}
