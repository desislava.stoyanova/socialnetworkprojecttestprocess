package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.entities.*;
import com.gaming.project.gamingproject.exceptions.EntityNotFoundException;
import com.gaming.project.gamingproject.models.RegisterUserModel;
import com.gaming.project.gamingproject.models.UserModel;
import com.gaming.project.gamingproject.models.UserRequestCheckModel;
import com.gaming.project.gamingproject.repositories.UserRepository;
import com.gaming.project.gamingproject.services.contracts.*;
import lombok.Setter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.beans.FeatureDescriptor;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Setter
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private RequestService requestService;
    private FriendService friendService;
    private LoginService loginService;
    private AuthoritiesService authoritiesService;
    private LikeCommentService likeCommentService;
    private ConfirmationTokenService tokenService;
    private ModelMapper mapper;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, RequestService requestService, FriendService friendService,
                           LoginService loginService, AuthoritiesService authoritiesService,
                           ConfirmationTokenService tokenService, ModelMapper mapper) {
        this.userRepository = userRepository;
        this.requestService = requestService;
        this.friendService = friendService;
        this.loginService = loginService;
        this.authoritiesService = authoritiesService;
        this.tokenService = tokenService;
        this.mapper = mapper;
    }

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void addUser(User user) {
        userRepository.save(user);
    }

    @Override
    public void createUser(RegisterUserModel registerUserModel) {
        loginService.createLogin(registerUserModel);
        Login login = loginService.findByUsername(registerUserModel.getLoginUsername());

        User user = new User();
        user.setFirstName(registerUserModel.getFirstName());
        user.setLastName(registerUserModel.getLastName());
        user.setEmail(registerUserModel.getEmail());
        user.setAge(registerUserModel.getAge());
        user.setGender(registerUserModel.getGender());
        user.setCountry(registerUserModel.getCountry());
        user.setLogin(login);
        user.setRegisterTime(LocalDateTime.now());
        if (registerUserModel.getGender().equals(GenderType.MALE)) {
            user.setPicture("avatar7.png");
        } else if (registerUserModel.getGender().equals(GenderType.OTHER)) {
            user.setPicture("avatar6.png");
        } else {
            user.setPicture("avatar3.png");
        }
        userRepository.save(user);
    }

    @Override
    public void sendRequest(int currUser, Principal principal) {
        User receiver = userRepository.findById(currUser)
                .orElseThrow(() -> new EntityNotFoundException("User does not exist"));
        User sender = getUser(principal.getName());
        if (requestService.doesRequestExist(sender, receiver)) {
            throw new IllegalArgumentException("Already exist");
        }
        requestService.createRequest(new Request(receiver, sender));
    }

    @Override
    public void addAsFriend(int currUser, Principal principal) {
        User sender = userRepository.findById(currUser)
                .orElseThrow(() -> new EntityNotFoundException("User does not exist"));
        User receiver = getUser(principal.getName());
        if (!requestService.doesRequestExist(sender, receiver)) {
            throw new EntityNotFoundException();
        }
        requestService.deleteRequest(sender, receiver);
        Friend friendShip = new Friend(receiver, sender);
        Friend friendShip2 = new Friend(sender, receiver);
        friendService.createFriendship(friendShip);
        friendService.createFriendship(friendShip2);
    }

    @Override
    public void removeFromFriends(int currUser, Principal principal) {
        User user = getUser(principal.getName());
        User user2 = userRepository.findById(currUser)
                .orElseThrow(() -> new EntityNotFoundException("User does not exist"));

        if (friendService.doesFriendshipExist(user, user2)) {
            friendService.deleteFriendShip(user, user2);
        } else if (friendService.doesFriendshipExist(user2, user)) {
            friendService.deleteFriendShip(user2, user);
        } else {
            throw new EntityNotFoundException();
        }
    }

    @Override
    public void removeFriendRequest(int currUser, int wantedUser) {
        User user = userRepository.findById(currUser)
                .orElseThrow(() -> new EntityNotFoundException("User does not exist"));
        User user2 = userRepository.findById(wantedUser)
                .orElseThrow(() -> new EntityNotFoundException("User does not exist"));

        if (requestService.doesRequestExist(user, user2) || requestService.doesRequestExist(user2, user)) {
            requestService.deleteRequest(user2, user);
        } else {
            throw new EntityNotFoundException("No such request.");
        }
    }

    @Override
    public List<User> findAllByGenderType(GenderType genderType) {
        return userRepository.findByGenderEquals(genderType);
    }

    @Override
    public Page<UserModel> userPagination(Pageable pageable, Principal principal) {
        User user = getUser(principal.getName());
        return userRepository.findAll(pageable, principal.getName())
                .map(el -> convertDtoCheck(el, user));
    }

    @Override
    public UserModel findByIdModel(int id) {
        return convertDto(userRepository.findById(id).orElseThrow(EntityNotFoundException::new));
    }

    @Override
    public User findById(int id) {
        return userRepository.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public Long countUsers() {
        return userRepository.count();
    }

    @Override
    public List<UserModel> getAllUsersByFirstNameOrLastName(String firstName, String lastName) {
        return convertListToDto(userRepository.findByFirstNameStartingWithOrLastNameStartingWith(firstName, lastName));
    }

    @Override
    public UserRequestCheckModel findUserById(int id, Principal principal) {
        User loggedUser = getUser(principal.getName());
        return convertToEntityFriendshipCheck
                (userRepository.findById(id).orElseThrow(EntityNotFoundException::new), loggedUser);
    }

    @Override
    public User findByEmailIgnoreCase(String email) {
        return userRepository.findByEmailIgnoreCase(email);
    }

    @Override
    public ConfirmationToken createUserAndToken(RegisterUserModel userModel) {
        loginService.createLogin(userModel);
        Login login = loginService.findByUsername(userModel.getLoginUsername());
        ConfirmationToken confirmationToken = new ConfirmationToken
                (convertFromRegisterModelToEntity(userModel, login));
        tokenService.createToken(confirmationToken);

        return confirmationToken;
    }

    @Override
    public String getUserRole(Authentication principal) {
        String userRole = principal.getAuthorities().toString();
        int firstDel = userRole.indexOf("_");
        userRole = userRole.substring(firstDel + 1, userRole.length() - 1);
        return userRole;
    }

    @Transactional
    @Override
    public void deleteUserById(Integer userId) {
        if (!userRepository.existsById(userId)) {
            throw new EntityNotFoundException("User does not exist !");
        }
        User user = findById(userId);
        final String username = user.getLogin().getUsername();
        likeCommentService.deleteCommentLikesByUserId(userId);
        authoritiesService.deleteAuthority(username);
        tokenService.deleteTokenByUserId(userId);
//        System.out.println(likeCommentService);
        userRepository.delete(user);
    }

    @Override
    public UserModel editUser(UserModel userModel) {
        User user = userRepository.findById(userModel.getId()).orElse(null);
        copyNonNullProperties(convertToEntity(userModel), user);
        return convertDto(userRepository.save(user));
    }

    @Override
    public User getUser(String userName) {
        return userRepository.findFirstByLoginUsername(userName);
    }

    static void copyNonNullProperties(Object src, Object target) {
        BeanUtils.copyProperties(src, target, getNullPropertyNames(src));
    }

    private static String[] getNullPropertyNames(Object source) {
        final BeanWrapper src = new BeanWrapperImpl(source);
        return Stream.of(src.getPropertyDescriptors())
                .map(FeatureDescriptor::getName)
                .filter(propertyName -> src.getPropertyValue(propertyName) == null || src.getPropertyValue(propertyName).equals(""))
                .toArray(String[]::new);
    }

    private List<UserModel> convertListToDto(List<User> users) {
        return users.stream()
                .map(this::convertDto)
                .collect(Collectors.toList());
    }

    private UserModel convertDtoCheck(User user, User loggedUser) {
        UserModel model = mapper.map(user, UserModel.class);
        if ((requestService.doesRequestExist(user, loggedUser) || requestService.doesRequestExist(loggedUser, user)) ||
                (friendService.doesFriendshipExist(user, loggedUser) || friendService.doesFriendshipExist(loggedUser, user))) {
            model.setHasFriend(true);
        }
        return model;
    }

    private UserModel convertDto(User user) {
        return mapper.map(user, UserModel.class);
    }

    private User convertToEntity(UserModel userModel) {
        return mapper.map(userModel, User.class);
    }

    private UserRequestCheckModel convertToEntityFriendshipCheck(User user, User principal) {
        UserRequestCheckModel model = new UserRequestCheckModel();
        if (requestService.doesRequestExist(principal, user) || requestService.doesRequestExist(user, principal)) {
            model.setRequestType(RequestType.PENDING);
        } else if (friendService.doesFriendshipExist(principal, user)
                || friendService.doesFriendshipExist(user, principal)) {
            model.setRequestType(RequestType.ACCEPTED);
        } else {
            model.setRequestType(RequestType.EMPTY);
        }
        return model;
    }

    private User convertFromRegisterModelToEntity(RegisterUserModel model, Login login) {
        User user = mapper.map(model, User.class);
        user.setLogin(login);
        user.setRegisterTime(LocalDateTime.now());
        if (user.getGender().equals(GenderType.MALE)) {
            user.setPicture("avatar7.png");
        } else if (user.getGender().equals(GenderType.OTHER)) {
            user.setPicture("avatar6.png");
        } else {
            user.setPicture("avatar3.png");
        }
        return user;
    }

    public void setAuthoritiesService(AuthoritiesService authoritiesService) {
        this.authoritiesService = authoritiesService;
    }

    public void setLoginService(LoginService loginService) {
        this.loginService = loginService;
    }

    public void setRequestService(RequestService requestService) {
        this.requestService = requestService;
    }

    @Autowired
    public void setLikeCommentService(LikeCommentService likeCommentService) {
        this.likeCommentService = likeCommentService;
    }
}
