package com.gaming.project.gamingproject.models;

import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PostVisualisationModel {

    private Integer id;
    private String text;
    private String userFirstName;
    private String userLastName;
    private Integer totalLikes;
    private List<CommentVisualizationModel> comments;
    private String userPicture;
    private boolean isLiked;
    private String creationTime;
    private String contentPicture;
    private String userLoginUsername;
    private String videoId;
    private String photo;
}
