package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.entities.Like;
import com.gaming.project.gamingproject.entities.Post;
import com.gaming.project.gamingproject.entities.User;
import com.gaming.project.gamingproject.exceptions.EntityNotFoundException;
import com.gaming.project.gamingproject.models.LikeModel;
import com.gaming.project.gamingproject.repositories.LikeRepository;
import com.gaming.project.gamingproject.services.contracts.LikeService;
import com.gaming.project.gamingproject.services.contracts.PostService;
import com.gaming.project.gamingproject.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;

@Service
public class LikeServiceImpl implements LikeService {

    private LikeRepository likeRepository;
    private PostService postService;
    private UserService userService;

    @Autowired
    public LikeServiceImpl(LikeRepository likeRepository, PostService postService, UserService userService) {
        this.likeRepository = likeRepository;
        this.postService = postService;
        this.userService = userService;
    }

    public LikeServiceImpl(LikeService likeService) {

    }

    @Override
    public boolean existsByPostIdAndUserId(int postId, int userId) {
        return likeRepository.existsByPostIdAndUserId(postId, userId);
    }

    @Override
    public void createLike(Like like) {
        likeRepository.save(like);
    }

    @Override
    public Like getByUserIdAndPostId(int userId, int likeId) {
        return likeRepository.findByUserIdAndPostId(userId,likeId);
    }

    @Override
    public Integer addLikeToPost(LikeModel likeModel, Principal principal) {
        Post post = postService.getById(likeModel.getPostId());
        User user = userService.getUser(principal.getName());

        if (existsByPostIdAndUserId(post.getId(), user.getId())) {
            throw new EntityNotFoundException("Not correct exception");
        }

        Like like = new Like(post, user);

        createLike(like);
        post.setTotalLikes(post.getTotalLikes() + 1);
        postService.createPost(post);
        return post.getTotalLikes();
    }

    @Override
    public Integer dislike(LikeModel likeModel, Principal principal) {
        Post post = postService.getById(likeModel.getPostId());
        User user = userService.getUser(principal.getName());

        if (!existsByPostIdAndUserId(post.getId(), user.getId())) {
            throw new EntityNotFoundException("Not correct exception");
        }
        Like like = getByUserIdAndPostId(user.getId(), likeModel.getPostId());
        likeRepository.delete(like);
        post.setTotalLikes(post.getTotalLikes() - 1);
        postService.createPost(post);
        return post.getTotalLikes();
    }

    public void setPostService(PostService postService) {
        this.postService = postService;
    }
}
