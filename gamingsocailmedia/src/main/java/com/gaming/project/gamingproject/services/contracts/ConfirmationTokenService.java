package com.gaming.project.gamingproject.services.contracts;

import com.gaming.project.gamingproject.entities.ConfirmationToken;

public interface ConfirmationTokenService {

    void createToken(ConfirmationToken confirmationToken);

    ConfirmationToken findByConfirmationToken(String confirmationToken);

    void deleteTokenByUserId(Integer userId);
}
