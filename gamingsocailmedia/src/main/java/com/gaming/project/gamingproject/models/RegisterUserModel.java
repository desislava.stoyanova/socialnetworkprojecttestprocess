package com.gaming.project.gamingproject.models;

import com.gaming.project.gamingproject.entities.GenderType;
import com.gaming.project.gamingproject.entities.Login;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class RegisterUserModel {

    @Size(min = 2, max = 24)
    private String loginUsername;

    @Size(min = 6, max = 40)
    private String loginPassword;

    @Size(min = 6, max = 40)
    private String passwordConfirmation;

    @Size(min = 5, max = 34)
    private String email;

    @Size(min = 2, max = 24)
    private String firstName;

    @Size(min = 2, max = 24)
    private String lastName;

    private Integer age;

    @NotNull
    private GenderType gender;

    @NotNull
    private String country;

    private String description;

    private Login login;

    private String picture;
}
