package com.gaming.project.gamingproject.repositories;

import com.gaming.project.gamingproject.entities.ConfirmationToken;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConfirmationTokenRepository extends CrudRepository<ConfirmationToken, String> {

    ConfirmationToken findByConfirmationToken(String confirmationToken);

    boolean existsByConfirmationToken(String confirmationToken);

    void deleteByUserId(Integer userId);

    boolean existsByUserId(int userId);
}
