package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.entities.Login;
import com.gaming.project.gamingproject.models.LoginModel;
import com.gaming.project.gamingproject.models.RegisterUserModel;
import com.gaming.project.gamingproject.repositories.LoginRepository;
import com.gaming.project.gamingproject.services.contracts.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LoginServiceImpl implements LoginService {

    private LoginRepository loginRepository;
    private UserDetailsManager userDetailsManager;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public LoginServiceImpl(LoginRepository loginRepository, UserDetailsManager userDetailsManager, PasswordEncoder passwordEncoder) {
        this.loginRepository = loginRepository;
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
    }

    public LoginServiceImpl(LoginRepository loginRepository) {
        this.loginRepository = loginRepository;
    }

    @Override
    public Login findByUsername(String username) {
        return loginRepository.findById(username).orElse(null);
    }

    @Override
    public void createLogin(RegisterUserModel registerUserModel) {
        LoginModel loginModel = new LoginModel();
        loginModel.setUsername(registerUserModel.getLoginUsername());
        loginModel.setPassword(registerUserModel.getLoginPassword());
        loginModel.setPasswordConfirmation(registerUserModel.getPasswordConfirmation());
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        loginModel.getUsername(), passwordEncoder.encode(loginModel.getPassword()), false,
                        true, true, true, authorities);

        userDetailsManager.createUser(newUser);
    }
}
