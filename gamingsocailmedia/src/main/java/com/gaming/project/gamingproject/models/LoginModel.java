package com.gaming.project.gamingproject.models;

import com.gaming.project.gamingproject.entities.Authorities;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginModel {

    private int id;

    private String username;

    private String password;

    private String passwordConfirmation;

    private Authorities role;
}
