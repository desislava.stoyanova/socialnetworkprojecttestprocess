package com.gaming.project.gamingproject.services.contracts;

import com.gaming.project.gamingproject.entities.Login;
import com.gaming.project.gamingproject.models.RegisterUserModel;

public interface LoginService {

    Login findByUsername(String username);

    void createLogin(RegisterUserModel registerUserModel);
}
