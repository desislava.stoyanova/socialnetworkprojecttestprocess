package com.gaming.project.gamingproject.entities;

public enum RequestType {
    ACCEPTED,
    PENDING,
    EMPTY
}
