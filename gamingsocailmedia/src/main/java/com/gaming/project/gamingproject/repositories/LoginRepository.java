package com.gaming.project.gamingproject.repositories;

import com.gaming.project.gamingproject.entities.Login;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LoginRepository extends CrudRepository<Login, String> {
}
