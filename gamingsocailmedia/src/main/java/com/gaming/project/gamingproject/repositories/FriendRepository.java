package com.gaming.project.gamingproject.repositories;

import com.gaming.project.gamingproject.entities.Friend;
import com.gaming.project.gamingproject.entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FriendRepository extends CrudRepository<Friend, Integer> {

    boolean existsBySenderIdAndReceiverId(int senderId, int receiverId);

    void deleteBySenderAndReceiver(User sender, User receiver);

    List<Friend> findAllByReceiver_Login_Username(String userLoginUsername);
}
