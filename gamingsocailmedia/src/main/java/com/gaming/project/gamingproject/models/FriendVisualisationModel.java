package com.gaming.project.gamingproject.models;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FriendVisualisationModel {

    private String senderFirstName;
    private String senderLastName;
    private String senderLoginUserName;
    private String senderPicture;
    private String receiverPicture;
}
