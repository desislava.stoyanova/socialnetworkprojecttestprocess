package com.gaming.project.gamingproject.repositories;

import com.gaming.project.gamingproject.entities.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer> {

    void deleteAllByPostId(int postId);

    Page<Comment> findAllByUserId(Pageable pageable, Integer userId);
}
