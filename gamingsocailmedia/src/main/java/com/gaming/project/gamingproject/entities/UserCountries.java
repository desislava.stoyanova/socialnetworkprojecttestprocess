package com.gaming.project.gamingproject.entities;

public enum UserCountries {
    Bulgaria,
    Serbia,
    Germany,
    France,
    Sweden,
    Italy,
    Denmark,
    Andorra,
    Austria,
    Bahamas,
    Belgium,
    Poland,
    Romania,
    Belarus,
    Estonia,
    Switzerland,
    Moldova,
    Montenegro,
    Cyprus,
    Luxembourg,
    Vatican,
    ;
}
