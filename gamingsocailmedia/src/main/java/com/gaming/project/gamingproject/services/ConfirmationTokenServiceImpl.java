package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.entities.ConfirmationToken;
import com.gaming.project.gamingproject.exceptions.EntityNotFoundException;
import com.gaming.project.gamingproject.repositories.ConfirmationTokenRepository;
import com.gaming.project.gamingproject.services.contracts.ConfirmationTokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class ConfirmationTokenServiceImpl implements ConfirmationTokenService {

    private final ConfirmationTokenRepository tokenRepository;

    @Override
    public void createToken(ConfirmationToken confirmationToken) {
        tokenRepository.save(confirmationToken);
    }

    @Override
    public ConfirmationToken findByConfirmationToken(String confirmationToken) {
        if (!tokenRepository.existsByConfirmationToken(confirmationToken)) {
           throw new EntityNotFoundException("Entity Not Found !");
        }
        return tokenRepository.findByConfirmationToken(confirmationToken);
    }

    @Transactional
    @Override
    public void deleteTokenByUserId(Integer userId) {
        if (!tokenRepository.existsByUserId(userId)) {
            throw new EntityNotFoundException("Token not found!");
        }
        tokenRepository.deleteByUserId(userId);
    }
}
