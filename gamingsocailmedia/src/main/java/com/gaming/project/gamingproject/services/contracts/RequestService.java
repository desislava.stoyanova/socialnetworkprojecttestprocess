package com.gaming.project.gamingproject.services.contracts;

import com.gaming.project.gamingproject.entities.Request;
import com.gaming.project.gamingproject.entities.User;
import com.gaming.project.gamingproject.models.RequestVisualisationModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.security.Principal;

public interface RequestService {

    void createRequest(Request request);

    boolean doesRequestExist(User sender, User receiver);

    void deleteRequest(User sender, User receiver);

    Page<RequestVisualisationModel> getAllRequestsByUserName(Principal principal, Pageable pageable);
}
