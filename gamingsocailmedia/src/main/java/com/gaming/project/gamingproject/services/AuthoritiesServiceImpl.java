package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.repositories.AuthoritiesRepository;
import com.gaming.project.gamingproject.services.contracts.AuthoritiesService;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Service;

@Service
@Setter
@RequiredArgsConstructor
public class AuthoritiesServiceImpl implements AuthoritiesService {

    private final AuthoritiesRepository authoritiesRepository;

    public boolean doesAuthorityExist(String username) {
        return authoritiesRepository.existsById(username);
    }

    public void deleteAuthority(String username) {
        authoritiesRepository.deleteById(username);
    }
}
