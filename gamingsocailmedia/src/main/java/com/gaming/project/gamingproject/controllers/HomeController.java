package com.gaming.project.gamingproject.controllers;

import com.gaming.project.gamingproject.entities.ConfirmationToken;
import com.gaming.project.gamingproject.entities.GenderType;
import com.gaming.project.gamingproject.entities.User;
import com.gaming.project.gamingproject.entities.UserCountries;
import com.gaming.project.gamingproject.models.LoginModel;
import com.gaming.project.gamingproject.models.RegisterUserModel;
import com.gaming.project.gamingproject.services.EmailSenderService;
import com.gaming.project.gamingproject.services.contracts.ConfirmationTokenService;
import com.gaming.project.gamingproject.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
public class HomeController {

    @Value("spring.mail.username")
    private String mailUsername;

    private final UserService userService;
    private final EmailSenderService emailSenderService;
    private final ConfirmationTokenService tokenService;

    @Autowired
    public HomeController(UserService userService, EmailSenderService emailSenderService,
                          ConfirmationTokenService tokenService) {
        this.userService = userService;
        this.emailSenderService = emailSenderService;
        this.tokenService = tokenService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView passValues(ModelAndView modelAndView, RegisterUserModel user) {
        modelAndView.addObject("login", new LoginModel());
        modelAndView.addObject("user", user);
        modelAndView.addObject("countries", UserCountries.values());
        modelAndView.addObject("genders", GenderType.values());
        modelAndView.setViewName("index");
        return modelAndView;
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ModelAndView registerUserEmail(ModelAndView modelAndView, @Valid RegisterUserModel user,
                                          HttpServletRequest request) {
        User existingUser = userService.findByEmailIgnoreCase(user.getEmail());
        if(existingUser != null)
        {
            modelAndView.addObject("message","This email already exists!");
            modelAndView.setViewName("error");
        } else {
            ConfirmationToken token = userService.createUserAndToken(user);
            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setTo(user.getEmail());
            mailMessage.setSubject("Complete Registration!");
            mailMessage.setFrom(mailUsername);
            mailMessage.setText("To confirm your account, please click here : "
                    + request.getRequestURL().toString() + "account-verified?token=" + token.getConfirmationToken());

            emailSenderService.sendEmail(mailMessage);
            modelAndView.addObject("emailId", user.getEmail());

            modelAndView.setViewName("register-success");
        }
        return modelAndView;
    }

    @RequestMapping(value="/account-verified", method= {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView confirmUserAccount(ModelAndView modelAndView, @RequestParam("token")String confirmationToken) {
        ConfirmationToken token = tokenService.findByConfirmationToken(confirmationToken);
        System.out.println(token);
        if (token != null) {
            User user = userService.findByEmailIgnoreCase(token.getUser().getEmail());
            user.getLogin().setEnabled(true);
            userService.addUser(user);
            modelAndView.setViewName("account-verified");
        } else {
            modelAndView.addObject("message","The link is invalid or broken!");
            modelAndView.setViewName("error");
        }
        return modelAndView;
    }

    @GetMapping("/register-confirmation")
    public String registerConfirmation() {
        return "register-confirmation";
    }
}
