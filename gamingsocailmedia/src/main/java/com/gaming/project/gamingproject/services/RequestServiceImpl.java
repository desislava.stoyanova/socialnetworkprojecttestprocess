package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.entities.Request;
import com.gaming.project.gamingproject.entities.User;
import com.gaming.project.gamingproject.models.RequestVisualisationModel;
import com.gaming.project.gamingproject.repositories.RequestRepository;
import com.gaming.project.gamingproject.services.contracts.RequestService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;

@Service
public class RequestServiceImpl implements RequestService {

    private RequestRepository requestRepository;

    @Autowired
    public RequestServiceImpl(RequestRepository requestRepository) {
        this.requestRepository = requestRepository;
    }

    @Override
    public void createRequest(Request request) {
        requestRepository.save(request);
    }

    @Override
    public boolean doesRequestExist(User sender, User receiver) {
        return requestRepository.existsBySenderIdAndReceiverId(sender.getId(), receiver.getId());
    }

    @Transactional
    @Override
    public void deleteRequest(User sender, User receiver) {
        requestRepository.deleteBySenderAndReceiver(sender, receiver);
    }

    @Override
    public Page<RequestVisualisationModel> getAllRequestsByUserName(Principal principal, Pageable pageable) {
        return requestRepository.findAllByReceiverLoginUsername(principal.getName(), pageable)
                .map(this::convertRequest);
    }

    private RequestVisualisationModel convertRequest(Request request) {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(request, RequestVisualisationModel.class);
    }
}
