package com.gaming.project.gamingproject.controllers.rest;

import com.gaming.project.gamingproject.entities.User;
import com.gaming.project.gamingproject.models.*;
import com.gaming.project.gamingproject.services.contracts.CommentService;
import com.gaming.project.gamingproject.services.contracts.PostService;
import com.gaming.project.gamingproject.services.contracts.UserService;
import io.swagger.annotations.*;
import io.swagger.models.auth.In;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
@Api(tags = { "User Rest Controller" })
public class UserRestController {

    private final UserService userService;
    private final PostService postService;

    @ApiOperation(value = "Retrieve UserModel by id", response = UserModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved user"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @GetMapping("/{id}")
    public UserModel getUser(/*@ApiParam(value = "Integer value by which user with current id will be retrieved",
            required = true)*/ @PositiveOrZero @NotNull @PathVariable Integer id) {
        return userService.findByIdModel(id);
    }

    @GetMapping("/check/{id}")
    public UserRequestCheckModel getUserButtonCheck(/*@ApiParam(value = "Integer value by which user with current id will be retrieved",
            required = true)*/ @PositiveOrZero @NotNull @PathVariable Integer id, Principal principal) {
        return userService.findUserById(id, principal);
    }

    @GetMapping("/getLogged")
    public UserModel getLoggedUser(Principal principal) {
        User user = userService.getUser(principal.getName());

        return userService.findByIdModel(user.getId());
    }

    @ApiOperation(value = "Get List of UserModels filtered by first name", response = List.class)
    @GetMapping("/filter")
    public List<UserModel> getAllUsersByFirstNameOrLastName(@ApiParam(value = "String value which produces list wish user" +
            " models if value is equal as first name of some user", required = true)
                                                      @RequestParam String firstName,
                                                            @RequestParam String lastName) {
        return userService.getAllUsersByFirstNameOrLastName(firstName, lastName);
    }

    @ApiOperation(value = "Get Page of user models", response = Page.class)
    @GetMapping("/page/")
    public Page<UserModel> getPage(Pageable pageable, Principal principal) {
        return userService.userPagination(pageable, principal);
    }

    @ApiOperation(value = "Add new user")
    @PostMapping("/")
    public void addUser(@ApiParam(value = "User object store in database table", required = true)
                            @RequestBody RegisterUserModel user) {
        userService.createUser(user);
    }

    @ApiOperation(value = "Add comment to post", response = CommentVisualizationModel.class)
    @PostMapping("/addComment")
    public CommentVisualizationModel addCommentToPost(@ApiParam(value = "Comment model which will be saved in database",
            required = true) @RequestBody @Valid CommentModel commentModel, Principal principal) {
        return postService.addCommentToPost(commentModel, principal);
    }

    @ApiOperation(value = "Edit fields of the chosen user", response = UserModel.class)
    @PutMapping("/")
    public UserModel editUser(@ApiParam(value = "Passed edit user as model which will replace already existing" +
            " user in database", required = true) @RequestBody @Valid UserModel userModel) {
        return  userService.editUser(userModel);
    }

    @ApiOperation(value = "Edit fields of the chosen user", response = UserModel.class)
    @DeleteMapping("/{userId}")
    public void deleteUser(@PositiveOrZero @NotNull @PathVariable Integer userId) {
        userService.deleteUserById(userId);
    }
}