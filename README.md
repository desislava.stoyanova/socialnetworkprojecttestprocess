# GSM_Automation

This is a repository for the testing process of Telerik Academy's Final Project - Social Network.

* [Link to Trello board](https://trello.com/b/DG9AL2Jd/gsmqaprocess)
* [Link to TestRail project](https://howtoqa.testrail.io/index.php?/projects/overview/5)

--------------------------------------------------------------------------------
## How to execute automated tests with Selenium WebDriver 

1. Run MailHog.exe
2. Open GSM project in Intellij IDE
3. Drop the data base (gaming scheme) and create new
4. Run GSM project
5. Manually register user with username: Admin and password: 123456 from UI and change it's role to ROLE_ADMIN in gaming scheme
6. Run Runner from socialNetworkProjectTestProcess 


## Installing Trap Mailing Service - Mailhog

### Windows

1. Download latest release of Mailhog desktop application or use the version stored into our gitlab repo https://gitlab.com/desislava.stoyanova/socialnetworkprojecttestprocess/
in case it fits your OS (Windows 64-bit - MailHog_windows_amd64.exe)

  Navigate in your browser to https://github.com/mailhog/MailHog/releases/v1.0.0 and download the version you need.


2. With Mailhog downloaded you need to start the executable. Default settings will be shown in cmd looking like this:

  ```
	Using in-memory storage
	[SMTP] Binding to address: 0.0.0.0:1025
	[HTTP] Binding to address: 0.0.0.0:8025
	Serving under http://0.0.0.0:8025/
	Creating API v1 with WebPath:
	Creating API v2 with WebPath:
  ```

3. Alter in GamingSocialMedia - GSM developer project,

  Edit:

  ```
  /src/main/resources/application.properties
  ```

  Change the following settings:

  ```
  spring.mail.host=0.0.0.0
  spring.mail.port=1025
  spring.mail.properties.mail.smtp.starttls.enable=false
  spring.mail.properties.mail.smtp.starttls.required=false
  spring.mail.properties.mail.smtp.auth=true
  ```

  Save the file and try to register a new user and open Mailhog webpage by default at <http://127.0.0.1:8025/#>
  
### MacOS

1. Launch Terminal by pressing command+space, type terminal, and hit Enter key.

  Run:

  ```
  ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" < /dev/null 2> /dev/null
  ```

  OR Install:

  ```
  brew install mailhog
  ```

2. With Mailhog installed, you'll need to start it now by running this command:

  ```
  brew services start mailhog
  ```

  _To check if Mailhog is running, open <http://127.0.0.1:8025> in your internet browser._

3. Back in Terminal, we'll need to edit a Postfix config file. Postfix is a mail server and alternative to the widely-used Sendmail program. Run this command:

  ```
  sudo vim /etc/postfix/main.cf
  ```

4. Add this to the bottom of the config file:

  ```
  # For Mailhog
  myhostname = localhost
  relayhost = [localhost]:1025
  ```

  _If any of the previous keys exist in your configuration, comment them out._

5. To load your changes, stop and start Postfix with this command:

  ```
  sudo postfix stop
  ```

  ```
  sudo postfix start
  ```

6. Finally, send a test email to see if everything is working. Run this command:

  ```
  date | mail -s "Test Email" your_email@gmail.com
  ```

7. Alter in GamingSocialMedia - GSM developer project,

  Edit:

  ```
  /src/main/resources/application.properties
  ```

  Change the following settings:

  ```
  spring.mail.host=0.0.0.0
  spring.mail.port=1025
  spring.mail.properties.mail.smtp.starttls.enable=false
  spring.mail.properties.mail.smtp.starttls.required=false
  spring.mail.properties.mail.smtp.auth=true
  ```

  Save the file and try to register a new user and open Mailhog webpage by default at <http://127.0.0.1:8025/#>

--------------------------------------------------------------------------------

## Postman integration with Newman

### Prerequisites: Node.js installed

1. Install Newman in command line/terminal

```
npm install -g newman
```
2. Install newman-reporter-htmlextra (optional)

```
npm install -g newman-reporter-htmlextra
```

3. Navigate to the directory where the postman collection is saved. Note that all postman files should be in same directory.
4. Run the following command 

- without Newman-report:

```
newman run Gaming\ Social\ Media\ .postman_collection.json -e Local.postman_environment.json

```
- with Newman-report:

```
newman run Gaming_Social_Media_.postman_collection.json -e Local.postman_environment.json -r htmlextra
```

