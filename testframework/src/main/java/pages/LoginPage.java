package pages;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import core.JsonReader;
import core.Utils;
import models.GenderType;
import models.User;

public class LoginPage extends BasePage {
    private String confirmationUrl;

    //gets confirmation URL from mailHog inbox
    public void verifyEmailReceived() {
        JsonReader jsonReader = new JsonReader();
        JsonElement jsonResponse = jsonReader.httpGet("http://localhost:8025/api/v2/messages");

        JsonArray items = jsonResponse.getAsJsonObject().get("items").getAsJsonArray();
        JsonObject lastEmailItem = items.get(0).getAsJsonObject();

        JsonObject emailContent = lastEmailItem.get("Content").getAsJsonObject();

        JsonObject headers = emailContent.get("Headers").getAsJsonObject();
        String toEmail = headers.get("To").getAsJsonArray().get(0).getAsString();

        String bodyContent = emailContent.get("Body").getAsString();

        this.confirmationUrl = bodyContent.substring(bodyContent.indexOf("http"));
    }

    public void confirmationLinkLoaded() {
        this.driver.get(this.confirmationUrl);
    }

    public User getUserSignUpInformation(String userKey)
    {
        String randomString = Utils.getRandomString(6);
        String username = randomString;
        String password = randomString;
        String firstName = randomString;
        String lastName = randomString;
        String email = userKey.toLowerCase() + "email" + randomString + "@test.com";
        GenderType gender = GenderType.FEMALE;
        Integer age = 30;
        String country = "Bulgaria";

        return new User(username, password, firstName, lastName, email, age, gender, country);
    }
}
