package pages;

import core.Utils;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class BasePage {
    final WebDriver driver;

    public BasePage() {
        this.driver = Utils.getWebDriver();
    }

    public static void loadBrowser() {
        Utils.getWebDriver().get(Utils.getConfigPropertyByKey("base.url"));
    }

    public static void quitDriver() {
        Utils.tearDownWebDriver();
    }

    //############# CLICK #########

    public void clickElement(String key) {
        Utils.LOG.info("Clicking on element " + key);
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(key)));
        waitForElementVisible(key,10);
        element.click();
    }
    public void clickElementByXpath(String xpath) {
        Utils.LOG.info("Clicking on element " + xpath);
        WebElement element = driver.findElement(By.xpath(xpath));
        element.click();
    }

    //############# MODAL #########
    //driver switches from browser window to modal window and accepts it
    public void confirmModal() {
        Alert alert = driver.switchTo().alert();
        alert.accept();
    }

    //############# TYPE #########

    public void typeValueInField(String value, String field){
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
        element.sendKeys(value);
    }

    public void typeValueConfigKeyInField(String valueConfigKey, String field){
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
        String value = Utils.getConfigPropertyByKey(valueConfigKey);
        element.sendKeys(value);
    }

    public void clearField(String field){
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
        element.clear();
    }

    //############# SCROLL #########

    public void scrollToElement(String key) {
        Utils.LOG.info("Scrolling to element " + key);
        JavascriptExecutor jsx = (JavascriptExecutor)driver;
        String selector = Utils.getUIMappingByKey(key);
        String script = "window.scroll({ top: (document.evaluate(\"" + selector + "\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).getBoundingClientRect().top + window.scrollY, behavior: 'auto' });";
        jsx.executeScript(script, "");
    }

    //############# WAIT #########

    public void waitForElementVisible(String locator, int seconds){
        WebElement element= driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
        WebDriverWait wait= new WebDriverWait(driver,seconds);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    //############# ASSERT #########

    public void assertElementPresent(String locator){
        String xPathLocator = Utils.getUIMappingByKey(locator);
        assertElementPresentByXPathLocator(xPathLocator);
    }

    public void assertElementPresentByXPathLocator(String xPathLocator){
        WebDriverWait wait= new WebDriverWait(driver,5);
        WebElement webElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPathLocator)));
        Assert.assertNotNull(webElement);
    }

    public void assertElementNotPresentByXPathLocator(String xPathLocator){
        List<WebElement> webElements = driver.findElements(By.xpath(xPathLocator));
        Assert.assertEquals(0, webElements.size());
    }

    public void assertTextEqualsByConfigKey(String textConfigKey, String customText, String locator){
        String expectedText = Utils.getConfigPropertyByKey(textConfigKey);
        assertTextEquals(expectedText + customText, locator);
    }

    public void assertTextEqualsByConfigKeyAndElementText(String textConfigKey, String locator){
        assertTextEqualsByConfigKey(textConfigKey, locator);
    }

    public void assertTextEqualsByConfigKey(String textConfigKey, String locator){
        String expectedText = Utils.getConfigPropertyByKey(textConfigKey) ;
        assertTextEquals(expectedText, locator);
    }

    public void assertTextEquals(String expectedText, String locator) {
        WebDriverWait wait= new WebDriverWait(driver,30);
        String xpath = Utils.getUIMappingByKey(locator);
        WebElement webElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
        String textOfWebElement = webElement.getText();
        Utils.LOG.info("Expected text: " + expectedText + "; Actual text: " + textOfWebElement);
        Assert.assertEquals(expectedText, textOfWebElement);
    }

    public void assertTextContainsByConfigKey(String textConfigKey, String locator){
        String expectedText = Utils.getConfigPropertyByKey(textConfigKey) ;
        assertTextContains(expectedText, locator);
    }

    public void assertTextContains(String expectedText, String locator) {
        WebDriverWait wait= new WebDriverWait(driver,30);
        String xpath = Utils.getUIMappingByKey(locator);
        WebElement webElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
        Boolean textPresentInElement = wait.until(ExpectedConditions.textToBePresentInElement(webElement, expectedText));
        Assert.assertEquals(textPresentInElement, true);
    }

    public void assertCurrentPageUrl(String pageUrlConfigKey) {
        String baseUrl = Utils.getConfigPropertyByKey("base.url");
        String expectedPageUrl = baseUrl + Utils.getConfigPropertyByKey(pageUrlConfigKey);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        String actualPageUrl = (String) js.executeScript("return window.top.location.href.toString()");
        Utils.LOG.info("Expected page url: " + expectedPageUrl + "; Actual page url: " + actualPageUrl);
        Assert.assertEquals(expectedPageUrl, actualPageUrl);
    }

    public void assertElementIsNotPresent(String locator){
        Assert.assertEquals(0, driver.findElements(By.xpath(Utils.getUIMappingByKey(locator))).size());
    }

    public void assertElementIsNotVisible(String locator){
        Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))).getCssValue("display") == "none");
    }

    //############# PAGE #########

    public void refreshPage(String pageUrlConfigKey){
        String url = Utils.getConfigPropertyByKey(pageUrlConfigKey);
        driver.get(url);
        driver.navigate().refresh();
    }

    //############# NAVIGATE TO PAGE #########

    public void navigateToPage(String pageName) {
        String baseUrl = Utils.getConfigPropertyByKey("base.url");
        String pageUrl = Utils.getConfigPropertyByKey(pageName);
        driver.get(baseUrl + pageUrl);
    }
}