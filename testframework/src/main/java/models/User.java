package models;

public class User {
    private String username;

    private String password;

    private String firstName;

    private String lastName;

    private String email;

    private int age;

    private GenderType gender;

    private String country;

    public User(String username, String password, String firstName, String lastName, String email, int age, GenderType gender, String country) {
        this.setUsername(username);
        this.setPassword(password);
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setEmail(email);
        this.setAge(age);
        this.setGender(gender);
        this.setCountry(country);
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public int getAge() {
        return age;
    }

    public GenderType getGender() {
        return gender;
    }

    public String getCountry() {
        return country;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setGender(GenderType gender) {
        this.gender = gender;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}

