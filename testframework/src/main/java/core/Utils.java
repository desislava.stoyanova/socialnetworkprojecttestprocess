package core;

import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Utils {

	private static Properties uiMappings =  PropertiesManager.PropertiesManagerEnum.INSTANCE.getUiMappings();
	private static Properties configProperties =  PropertiesManager.PropertiesManagerEnum.INSTANCE.getConfigProperties();
	public static final Logger LOG = LogManager.getRootLogger();

	public static WebDriver getWebDriver() {
		LOG.info("Initializing WebDriver");
		return CustomWebDriverManager.CustomWebDriverManagerEnum.INSTANCE.getDriver();
	}

	public static void tearDownWebDriver() {
		LOG.info("Quitting WebDriver");
		CustomWebDriverManager.CustomWebDriverManagerEnum.INSTANCE.quitDriver();
	}

	public static String getUIMappingByKey(String key) {
		String value = uiMappings.getProperty(key);
		return value != null ? value : key;
	}

	public static Properties getConfigProperties() {
		return PropertiesManager.PropertiesManagerEnum.INSTANCE.getConfigProperties();
	}

	public static String getConfigPropertyByKey(String key){
		return configProperties.getProperty(key);
	}

	public static String getRandomString(int n) {

		// chose a Character random from this String
		String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
				+ "0123456789"
				+ "abcdefghijklmnopqrstuvxyz";

		// create StringBuffer size of AlphaNumericString
		StringBuilder sb = new StringBuilder(n);

		for (int i = 0; i < n; i++) {

			// generate a random number between 0 to AlphaNumericString variable length
			int index
					= (int)(AlphaNumericString.length()
					* Math.random());

			// add Character one by one in end of sb
			sb.append(AlphaNumericString
					.charAt(index));
		}

		return sb.toString();
	}
	// gets files within given directory and converts their content into Properties object by combining it
	public static Properties convertDirectoryFilesToProperties(String directoryPath) {
		List<String> uiMappingsFilePaths = new ArrayList<String>();
		File[] files = new File(directoryPath).listFiles();
		for (File file : files) {
			if (file.isFile()) {
				uiMappingsFilePaths.add(file.getAbsolutePath());
			}
		}

		Properties props = new Properties();

		for (String uiMap : uiMappingsFilePaths) {
			try {
				// FileInputStream returns the content of the uiMap
				props.load(new FileInputStream(uiMap));
			} catch (IOException ex) {
				Utils.LOG.error("Loading properties failed!");
			}
		}

		return props;
	}
}
