package core;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesManager {
	public enum PropertiesManagerEnum {
		INSTANCE;
		private Properties configProperties = null;
		private Properties uiMappings = null;
		private static final String UI_MAP_PROPERTIES_DIRECTORY = "src/test/resources/mappings/";
		private static final String CONFIG_PROPERTIES_DIRECTORY = "src/test/resources/configs/";

		public Properties getConfigProperties() {
			return configProperties = Utils.convertDirectoryFilesToProperties(CONFIG_PROPERTIES_DIRECTORY);
		}

		public Properties getUiMappings() { return uiMappings = Utils.convertDirectoryFilesToProperties(UI_MAP_PROPERTIES_DIRECTORY); }
	}
}
