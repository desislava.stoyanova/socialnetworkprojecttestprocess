package core;

import java.io.IOException;

import com.google.gson.JsonElement;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;


public class JsonReader {
    //Sends HTTP request to mailHog's API and returns Json response containing e-mail with confirmation link
    public JsonElement httpGet(String url) {
        JsonElement jsonResponse = null;

        try {
            CloseableHttpClient httpClient = HttpClientBuilder.create().build();
            HttpGet request = new HttpGet(url);
            request.addHeader("content-type", "application/json");
            HttpResponse result = httpClient.execute(request);
            String json = EntityUtils.toString(result.getEntity(), "UTF-8");

            com.google.gson.Gson gson = new com.google.gson.Gson();
            jsonResponse = gson.fromJson(json, JsonElement.class);
        }
        catch(IOException e) {
            e.printStackTrace();
        }
        return jsonResponse;
    }
}
