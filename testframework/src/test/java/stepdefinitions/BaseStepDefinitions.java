package stepdefinitions;

import org.jbehave.core.annotations.AfterStories;
import org.jbehave.core.annotations.BeforeStories;
import org.jbehave.core.annotations.BeforeStory;
import pages.BasePage;

public class BaseStepDefinitions {
    @BeforeStories
    public void beforeStories() {
        BasePage.loadBrowser();
    }

    @BeforeStory
    public void beforeStory() {
        BasePage.loadBrowser();
    }

    @AfterStories
    public void afterStories() {
        BasePage.quitDriver();
    }
}

