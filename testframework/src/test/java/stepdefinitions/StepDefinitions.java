package stepdefinitions;

import core.Utils;
import models.User;
import org.jbehave.core.annotations.*;
import org.jbehave.core.model.ExamplesTable;
import org.openqa.selenium.WebDriver;
import pages.BasePage;
import pages.LoginPage;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class StepDefinitions extends stepdefinitions.BaseStepDefinitions {
    BasePage basePage = new BasePage();
    LoginPage loginPage = new LoginPage();

    String currentRandomString;
    Hashtable<String, User> users = new Hashtable<String, User>();

    @Given("Click $element element")
    @When("Click $element element")
    @Then("Click $element element")
    @Aliases(values = {"Peter clicks on $element", "User clicks on $element field", "User clicks on $element",
            "Clicks $element element", "Emma clicks on $element", "Admin clicks on $element"})
    public void clickElement(String element){
        basePage.clickElement(element);
    }

    @When("Clicks twice $element element")
    @Aliases(values = {"Admin clicks twice on $element"})
    public void clickElementTwice(String element){
        basePage.clickElement(element);
        basePage.clickElement(element);
    }

    @When("Confirm modal window")
    public void confirmModal() {
        basePage.confirmModal();
    }

    @Given("Click in $element field")
    @When("Click in $element field")
    @Then("Click in $element field")
    @Aliases(values = {"Peter clicks on $element field", "Peter clicks in $element field"})
    public void clickInField(String element){
        basePage.clickElement(element);
    }

    @When("Enter $userKey username in $fieldName field")
    @Aliases(values = {"Admin enters $username in $field"})
    public void typeCurrentUserUserNameInField(String userKey, String field) {
        String username = users.get(userKey).getUsername();
        basePage.typeValueInField(username, field);
    }

    @When("Enter $userKey password in $fieldName field")
    public void typeCurrentUserPasswordInField(String userKey, String field) {
        basePage.typeValueInField(users.get(userKey).getPassword(), field);
    }

    @When("Enter $userKey first name in $fieldName field")
    public void typeCurrentUserFirstNameInField(String userKey, String field) {
        basePage.typeValueInField(users.get(userKey).getFirstName(), field);
    }

    @When("Enter $userKey last name in $fieldName field")
    public void typeCurrentUserLastNameInField(String userKey, String field) {
        basePage.typeValueInField(users.get(userKey).getLastName(), field);
    }

    @When("Enter $userKey email in $fieldName field")
    public void typeCurrentUserEmailInField(String userKey, String field) {
        basePage.typeValueInField(users.get(userKey).getEmail(), field);
    }

    @When("Enter $userKey age in $fieldName field")
    public void typeCurrentUserAgeInField(String userKey, String field) {
        basePage.typeValueInField(Integer.toString(users.get(userKey).getAge()), field);
    }

    @Given("Type $valueConfigKey in $fieldName field")
    @When("Type $valueConfigKey in $fieldName field")
    @Then("Type $valueConfigKey in $fieldName field")
    @Aliases(values = {"Types $valueConfigKey in $fieldName field", "Admin types $valueConfigKey in $fieldName field"})
    public void typeInField(String valueConfigKey, String field){
        basePage.typeValueConfigKeyInField(valueConfigKey, field);
    }

    @When("Set random string with $length characters at $name field")
    public void typeRandomStringInField(Integer length, String field){
        this.currentRandomString = Utils.getRandomString(length);
        basePage.typeValueInField(this.currentRandomString, field);
    }

    @Given("Scroll to $element")
    @When("Scroll to $element")
    @Then("Scroll to $element")
    @Aliases(values = {"Peter scrolls to $element", "Emma scrolls to $element", "Admin scrolls to $element"})
    public void scrollToElement(String element) {
        basePage.scrollToElement(element);
    }


    @Given("Wait until element is visible")
    @When("Wait until element is visible")
    @Then("Wait until element is visible")
    @Aliases(values={"When element is displayed", "Waits until Emma.profileArea is displayed", "Waits until Emma's profile is loaded", "Waits until confirm modal is shown",
            "Waits until public feed is displayed", "Waits until Peter's friend request is displayed", "Waits until his profile is loaded",
            "Waits until her profile is loaded","Waits until $element is visible", "Wait to load", "Admin waits for post to be deleted"})
    public void waitForElementVisible(){
        try {
            Thread.sleep(2 * 1000);
        }
        catch (InterruptedException e) { }
    }

    @Then("Assert combined $textConfigKey and $userKey email equals $locator")
    public void assertCombinedTextEqualsByConfigKey(String textConfigKey, String userKey, String locator) {
        String email = users.get(userKey).getEmail();
        basePage.assertTextEqualsByConfigKey(textConfigKey, email, locator);
    }

    @Then("Assert random string is identical to $locator")
    public void assertTextEqualsByRandomString(String locator) {
        basePage.assertTextEquals(this.currentRandomString, locator);
    }

    @Given("Assert $textConfigKey is equal to $locator by text")
    @When("Assert $textConfigKey is equal to $locator by text")
    @Then("Assert $textConfigKey is equal to $locator by text")
    public void assertTextEqualsByConfigKeyAndElementText(String textConfigKey, String locator) {
        String expectedText = Utils.getConfigPropertyByKey(textConfigKey);
        locator = String.format(locator, expectedText);
        basePage.assertTextEqualsByConfigKeyAndElementText(textConfigKey, locator);
    }

    @Given("Assert $textConfigKey equals $locator")
    @When("Assert $textConfigKey equals $locator")
    @Then("Assert $textConfigKey equals $locator")
    public void assertTextEqualsByConfigKey(String textConfigKey, String locator) {
        basePage.assertTextEqualsByConfigKey(textConfigKey, locator);
    }

    @Given("Assert $textConfigKey contains $locator")
    @When("Assert $textConfigKey contains $locator")
    @Then("Assert $textConfigKey contains $locator")
    @Aliases(values = {"Peter's $changedFirstName is changed to $newFirstName", "Peter's last name is changed to $newLastName"})
    public void assertTextContainsByConfigKey(String textConfigKey, String locator) {
        basePage.assertTextContainsByConfigKey(textConfigKey, locator);
    }

    @Then("Verify element $element with text $textConfigKey is presented")
    public void assertElementPresent(String locator, String textConfigKey) {
        String expectedText = Utils.getConfigPropertyByKey(textConfigKey);
        String xPathLocator = Utils.getUIMappingByKey(locator);
        xPathLocator = String.format(xPathLocator, expectedText);
        basePage.assertElementPresentByXPathLocator(xPathLocator);
    }

    @Then("Verify element $element with text $textConfigKey is not presented")
    public void assertElementNotPresent(String locator, String textConfigKey) {
        String expectedText = Utils.getConfigPropertyByKey(textConfigKey);
        String xPathLocator = Utils.getUIMappingByKey(locator);
        xPathLocator = String.format(xPathLocator, expectedText);
        basePage.assertElementNotPresentByXPathLocator(xPathLocator);
    }

    @When("Confirm element $element with username $textConfigKey is visible")
    @Then("Confirm element $element with username $textConfigKey is visible")
    public void assertElementPresentByUserKey(String locator, String userKey) {
        String username = this.users.get(userKey).getUsername();
        String xPathLocator = Utils.getUIMappingByKey(locator);
        xPathLocator = String.format(xPathLocator, username);
        basePage.assertElementPresentByXPathLocator(xPathLocator);
    }
    @Then("Assert element $element with username $textConfigKey is not visible")
    public void assertElementNotPresentByUserKey(String locator, String userKey) {
        String username = this.users.get(userKey).getUsername();
        String xPathLocator = Utils.getUIMappingByKey(locator);
        xPathLocator = String.format(xPathLocator, username);
        basePage.assertElementNotPresentByXPathLocator(xPathLocator);
    }
    @When("Admin clicks element $element for username $textConfigKey")
    public void clickOnElementByUsername(String locator, String userKey) {
        String username = this.users.get(userKey).getUsername();
        String xPathLocator = Utils.getUIMappingByKey(locator);
        xPathLocator = String.format(xPathLocator, username);
        basePage.clickElementByXpath(xPathLocator);
    }

    @Given("Assert $element is presented")
    @When("Assert $element is presented")
    @Then("Assert $element is presented")
    @Aliases(values={"Asserts $element is present", "$element is present"})
    public void assertElementPresent(String locator) {
        basePage.assertElementPresent(locator);
    }

    @Given("Assert $pageUrl is the current page")
    @When("Assert $pageUrl is the current page")
    @Then("Assert $pageUrl is the current page")
    @Aliases(values = {"User is on $pageUrl", "User is redirected to $pageUrl", "Peter is redirected to $pageUrl",
            "Emma is redirected to $pageUrl", "Peter is on $pageUrl", "Emma is on $pageUrl", "George is redirected to $pageUrl", "Anna is redirected to $pageUrl"})
    public void assertCurrentPageUrl(String pageUrlConfigKey) {
        basePage.assertCurrentPageUrl(pageUrlConfigKey);
    }

    @Given("Verify email is received from current user email account")
    public void verifyEmailReceived() {
        loginPage.verifyEmailReceived();
    }

    @Given("Confirm confirmation link is loaded")
    @When("Confirm confirmation link is loaded")
    @Then("Confirm confirmation link is loaded")
    public void confirmationLinkLoaded() {
        loginPage.confirmationLinkLoaded();
    }

    @Given("User is navigated to $loginPage.Url")
    @Aliases(values = {"George is navigated to $loginPage.Url", "Anna is navigated to $loginPage.Url"})
    public void navigateToPage(String pageName){
        basePage.navigateToPage(pageName);
    }

    @When("User $userKey sign up information is available")
    public void signUpInformationIsAvailable(String userKey) {
        User user = loginPage.getUserSignUpInformation(userKey);
        this.users.put(userKey, user);
    }

    @When("Assert $element is not present")
    @Then("Assert $element is not present")
    public void assertElementNotPresent(String locator) { basePage.assertElementIsNotPresent(locator);}

    @When("Assert $element is not visible")
    @Then("Assert $element is not visible")
    public void assertElementNotVisible(String locator) { basePage.assertElementIsNotVisible(locator);}

    @When("Peter refreshes the $page")
    public void refreshPage(String url){
        basePage.refreshPage(url); }

    @When("Peter clears $element")
    public void clearField(String locator) { basePage.clearField(locator);}

    @When("Admin clears $element")
    public void adminClearsField(String locator) { basePage.clearField(locator);}

    @When("Admin Types existing user: $username in $field field")
    public void getUserUserName(String userKey, String field) {
        String username = users.get(userKey).getUsername();
        basePage.typeValueInField(username, field);
    }
}
