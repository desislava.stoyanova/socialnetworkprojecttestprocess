Meta:
@like

Narrative:
As a logged-in user
Peter wants to like user's post
So that he can show his support

GivenStories:stories/01.LoginPage/S09_LoginValid.story

Scenario: Like user's post
Given Peter is on homePage
And Assert most.Recently.Created.Post is presented
When Click post.Like.Button element
And Wait until element is visible
Then Assert post.Dislike.Button is presented
And Assert one.Like equals post.Like.Count