Meta:
@comment

Narrative:
As a logged-in user
Peter wants to leave a comment on user's post
So that he can give his opinion on a subject

GivenStories:stories/01.LoginPage/S09_LoginValid.story

Scenario: Comment user's post
Given Peter is on homePage
And Assert most.Recently.Created.Post is presented
When Click in comment.Box field
And Set random string with 8 characters at comment.Box field
And Click post.Comment.Button element
Then Assert random string is identical to most.Recently.Created.Comment