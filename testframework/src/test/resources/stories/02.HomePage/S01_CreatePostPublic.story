Meta:
@post

Narrative:
As a logged-in user
Peter wants to create a post
So that he can share information on his feed

GivenStories:stories/01.LoginPage/S09_LoginValid.story

Scenario: Peter creates a public post
Given Peter is on homePage
When Type post.Creation.Text in post.Box field
And Click share.Button element
Then Verify element post with text post.Creation.Text is presented