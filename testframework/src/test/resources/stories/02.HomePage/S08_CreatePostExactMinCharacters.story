Meta:
@post

Narrative:
As a logged-in user
Peter wants to create a post with 1 character that is exact minimum boundary value for post body length
So that he can share information on his feed

GivenStories:stories/01.LoginPage/S09_LoginValid.story

Scenario: Create a post with characters that is exact min boundary value for post body
Given Peter is on homePage
When Set random string with 1 characters at post.Box field
And Click share.Button element
And Wait until element is visible
Given Assert most.Recently.Created.Post is presented
Then Assert random string is identical to most.Recently.Created.Post