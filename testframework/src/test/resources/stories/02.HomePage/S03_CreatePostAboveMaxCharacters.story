Meta:
@post

Narrative:
As a logged-in user
Peter wants to create a post with 256 characters above maximum boundary value for post body length
So that he can share information on his feed and correct the input

GivenStories:stories/01.LoginPage/S09_LoginValid.story

Scenario: Create a post with 256 characters above max boundary value for post body
Given Peter is on homePage
When Set random string with 256 characters at post.Box field
And Click share.Button element
And Assert post.Creation.Error is presented
Then Assert post.Creation.Error.Message contains post.Creation.Error
