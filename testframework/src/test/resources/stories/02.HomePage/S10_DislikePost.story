Meta:
@dislike

Narrative:
As a logged-in user
Peter wants to unlike previously liked user's post
So that he can show that he doesn't like it anymore

GivenStories:stories/01.LoginPage/S09_LoginValid.story

Scenario: Disike already liked user's post
Given Peter is on homePage
And Assert most.Recently.Created.Post is presented
When Click post.Dislike.Button element
And Wait until element is visible
Then Assert post.Like.Button is presented
And Assert zero.Likes equals post.Like.Count