Meta:
@post

Narrative:
As a logged-in user
Peter wants to try create a post with 0 character that is bellow minimum boundary value for post body length (without content)
So that he can correct the input

GivenStories:stories/01.LoginPage/S09_LoginValid.story

Scenario: Create a post without content
Given Peter is on homePage
When Click share.Button element
And Assert post.Creation.Error is presented
Then Assert post.Creation.Error.Message contains post.Creation.Error