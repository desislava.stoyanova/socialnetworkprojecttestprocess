Meta:
@post

Narrative:
As a logged-in user
Peter wants to create a private post
So that he can share information on his feed only with his friends

GivenStories:stories/01.LoginPage/S09_LoginValid.story

Scenario: Peter creates a private post
Given Peter is on homePage
When Types post.Creation.Text in post.Box field
And Click postVisibility.Button element
And Click private.select.option element
And Click share.Button element
Then Verify element post with text post.Creation.Text is presented

