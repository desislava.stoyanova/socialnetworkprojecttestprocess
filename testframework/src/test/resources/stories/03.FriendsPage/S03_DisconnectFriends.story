Meta:friends

Narrative:
As registered user
Peter wants to search for Emma's profile
So that he can disconnect with her

GivenStories:stories/01.LoginPage/S09_LoginValid.story
Scenario: Disconnect friends
Given Peter is on homePage
When Click userProfile.Button element
And Wait until element is visible
And Confirm element profilePageUsername with username Peter is visible
And Peter scrolls to recentlyConnected.friends
And Clicks recentlyConnectedUser.box element
And Waits until Emma's profile is loaded
And Confirm element profilePageUsername with username Emma is visible
And Asserts removeFriend.button is present
When Peter clicks on removeFriend.button
And Waits until confirm modal is shown
When Confirm modal window
And Asserts addFriend.Button is present
When Peter clicks on logout.Button
Then Peter is redirected to logOut.Page





