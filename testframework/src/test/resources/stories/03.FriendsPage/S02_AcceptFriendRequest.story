Meta:friends

Narrative:
As a registered user
Emma wants to login in the app
So that she can confirm Peter's request

GivenStories:stories/01.LoginPage/S10_LoginValidSecondUser.story
Scenario: Accept friend request
Given Emma is on homePage
And Asserts newFriend.badge is present
When Emma clicks on newFriend.badge
And Waits until Peter's friend request is displayed
And Asserts acceptFriendRequest.button is present
And Emma clicks on acceptFriendRequest.button
Then Assert acceptFriendRequest.button is not visible
When Emma clicks on logout.Button
Then Emma is redirected to logOut.Page