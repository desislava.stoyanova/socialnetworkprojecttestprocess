Meta:friends

Narrative:
As a registered user
Emma wants to log in the app
So that she can decline Peter's request

GivenStories:stories/03.FriendsPage/S01_SendFriendRequest.story,
             stories/01.LoginPage/S10_LoginValidSecondUser.story

Scenario: Decline a friend request
When Asserts newFriend.badge is present
When Emma clicks on newFriend.badge
And Waits until Peter's friend request is displayed
And Asserts declineFriendRequest.button is present
And Emma clicks on declineFriendRequest.button
When Asserts addFriend.button is present
When Emma clicks on logout.Button
Then Emma is redirected to logOut.Page