Meta:friends

Narrative:
As a registered user
Peter wants to search for user Emma
So that he can send her a friend request

GivenStories:stories/01.LoginPage/S09_LoginValid.story
Scenario: Send friend request
Given Peter is on homePage
Given Assert friends.Button is presented
When Peter clicks on friends.Button
And Peter clicks on search.Field
And Enter Emma username in search.Field field
Then Confirm element profileArea with username Emma is visible
And Peter clicks on addFriend.button
When Assert addFriend.button is not visible
When Peter clicks on viewProfile.button
Then Waits until Emma's profile is loaded
When Confirm element profilePageUsername with username Emma is visible
And Asserts pendingRequest.Button is present
When Peter clicks on logout.Button
Then Peter is redirected to logOut.Page
