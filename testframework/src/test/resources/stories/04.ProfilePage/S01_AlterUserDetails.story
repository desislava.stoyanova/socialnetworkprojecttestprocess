Meta:profilePage

Narrative:
As a registered user
Peter wants to navigate to his profile page
So that he can change his name

GivenStories:stories/01.LoginPage/S09_LoginValid.story

Scenario: User changes his profile name
Given Peter is on homePage
And userProfile.Button is present
When Peter clicks on userProfile.Button
And Waits until his profile is loaded
When editProfile.Button is present
And Peter clicks on editProfile.Button
And editorial.Window is present
When Peter clicks on firstName.Edit.Field
When Peter clears firstName.Edit.Field
And Types new.FirstName in firstName.Edit.Field field
When Peter clicks on lastName.Edit.Field
And Peter clears lastName.Edit.Field
And Types new.LastName in lastName.Edit.Field field
When Peter clicks on description.Edit.Field
And Peter clears description.Edit.Field
And Types new.Description in description.Edit.Field field
When Peter clicks on age.Edit.Field
And Peter clears age.Edit.Field
And Types new.Age in age.Edit.Field field
When Peter clicks on editorialWindow.Edit.Button
When Peter refreshes the currentPage
Then Peter's new.FirstName is changed to changed.FirstName
And Peter's new.LastName is changed to changed.LastName
And Peter's new.Description is changed to changed.Description
And Peter's new.Age is changed to changed.Age







