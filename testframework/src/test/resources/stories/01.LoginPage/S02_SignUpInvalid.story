Meta:
@signUp

Narrative:
As a user
I want to make invalid registration and see validation error messages
So that I can correct my input

Scenario: Make invalid registration
Given Scroll to signUp.Form
Given Click username.signUp.Input element
When Type currentUser.Invalid.UserName in username.signUp.Input field
Then Assert username.Invalid.signUp.ErrorMessageText equals username.Invalid.signUp.ErrorMessage
Given Click password.signUp.Input element
When Type currentUser.Invalid.Password in password.signUp.Input field
Then Assert password.Invalid.signUp.ErrorMessageText equals password.Invalid.signUp.ErrorMessage
Given Click confirmPassword.signUp.Input element
When Type currentUser.Invalid.Password in confirmPassword.signUp.Input field
Given Click firstName.signUp.Input element
When Type currentUser.Invalid.FirstName in firstName.signUp.Input field
Then Assert firstName.Invalid.signUp.ErrorMessageText equals firstName.Invalid.signUp.ErrorMessage
Given Click lastName.signUp.Input element
When Type currentUser.Invalid.LastName in lastName.signUp.Input field
Then Assert lastName.Invalid.signUp.ErrorMessageText equals lastName.Invalid.signUp.ErrorMessage
Given Click emailAddress.signUp.Input element
When Type currentUser.Invalid.Email in emailAddress.signUp.Input field
Given Scroll to age.signUp.Input
Given Click age.signUp.Input element
When Type currentUser.Invalid.Age in age.signUp.Input field
Then Assert age.Invalid.signUp.ErrorMessageText equals age.Invalid.signUp.ErrorMessage
Given Scroll to signUp.Button
Then Click signUp.Button element