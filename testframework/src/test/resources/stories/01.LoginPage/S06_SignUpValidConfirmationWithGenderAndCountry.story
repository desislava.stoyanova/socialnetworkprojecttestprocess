Meta:
@signUp

Narrative:
As a user
Emma wants to confirm her registration
So that she can login to her account

Scenario: Confirm valid registration
Given Verify email is received from current user email account
When Confirm confirmation link is loaded
Then Assert signUp.Confirmation.Text equals singUp.Confirmation