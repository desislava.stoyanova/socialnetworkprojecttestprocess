Meta:
@signUp

Narrative:
As a user
Peter wants to make a valid registration
So that he can have account

Scenario: Make valid registration for Peter
Given Scroll to signUp.Form
When User Peter sign up information is available
Given Click username.signUp.Input element
When Enter Peter username in username.signUp.Input field
Given Click password.signUp.Input element
When Enter Peter password in password.signUp.Input field
Given Click confirmPassword.signUp.Input element
When Enter Peter password in confirmPassword.signUp.Input field
Given Click firstName.signUp.Input element
When Enter Peter first name in firstName.signUp.Input field
Given Click lastName.signUp.Input element
When Enter Peter last name in lastName.signUp.Input field
Given Click emailAddress.signUp.Input element
When Enter Peter email in emailAddress.signUp.Input field
Given Click age.signUp.Input element
When Enter Peter age in age.signUp.Input field
Given Scroll to signUp.Button
Then Click signUp.Button element
Then Assert signUp.VerificationEmailSentText contains signUp.VerificationEmailSent