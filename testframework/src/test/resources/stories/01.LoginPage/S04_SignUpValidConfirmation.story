Meta:
@signUp

Narrative:
As a user
Peter wants to confirm his registration
So that he can login to his account

Scenario: Confirm valid registration
Given Verify email is received from current user email account
When Confirm confirmation link is loaded
Then Assert signUp.Confirmation.Text equals singUp.Confirmation