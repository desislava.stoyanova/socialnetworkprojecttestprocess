Meta:
@signUp

Narrative:
As a user
I want to register without entering input data
So that I can correct my input

Scenario: Make invalid registration with blank fields
Given Scroll to signUp.Button
Then Click signUp.Button element
Then Assert basePage is the current page