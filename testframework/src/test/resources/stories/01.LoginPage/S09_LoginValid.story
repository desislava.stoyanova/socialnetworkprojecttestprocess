Meta:
@login

Narrative:
As a registered user
Peter wants to login successfully
So that he can use the application

Scenario: Successful login Peter
Given Scroll to login.Form
Given Click username.Login.Input element
When Enter Peter username in username.Login.Input field
Given Click password.Login.Input element
When Enter Peter password in password.Login.Input field
Then Click login.Button element
Then Peter is on homePage
