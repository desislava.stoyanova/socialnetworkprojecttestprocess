Meta:
@login

Narrative:
As a user
I want to login with invalid credentials
So that I can check the behavior of the application

Scenario: Unsuccessful login
Given Scroll to login.Form
Given Click username.Login.Input element
When Type currentUser.Invalid.UserName in username.Login.Input field
Given Click password.Login.Input element
When Type currentUser.Invalid.Password in password.Login.Input field
Then Click login.Button element
Then Assert login.ErrorText equals login.Error.Message