Meta:
@login

Narrative:
As a user
I want to try login with wrong password
So that I can check if there is warning message

Scenario: Unsuccessful login with wrong password
Given Scroll to login.Form
Given Click username.Login.Input element
When Type currentUser.UserName in username.Login.Input field
Given Click password.Login.Input element
When Type currentUser.Invalid.Password in password.Login.Input field
Then Click login.Button element
Then Assert login.ErrorText equals login.Error.Message