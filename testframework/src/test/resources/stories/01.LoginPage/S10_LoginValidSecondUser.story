Meta:
@login

Narrative:
As a registered user
Emma wants to login successfully
So that she can use the application

Scenario: Successful login second user Emma
Given Scroll to login.Form
Given Click username.Login.Input element
When Enter Emma username in username.Login.Input field
Given Click password.Login.Input element
When Enter Emma password in password.Login.Input field
Then Click login.Button element
Then Emma is on homePage
