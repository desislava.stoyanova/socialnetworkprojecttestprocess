Meta:
@signUp

Narrative:
As a user
Emma wants to make a valid registration with gender and country selected
So that she can have an account

Scenario: Make valid registration with gender and country for Emma
Given Scroll to signUp.Form
When User Emma sign up information is available
Given Click username.signUp.Input element
When Enter Emma username in username.signUp.Input field
Given Click password.signUp.Input element
When Enter Emma password in password.signUp.Input field
Given Click confirmPassword.signUp.Input element
When Enter Emma password in confirmPassword.signUp.Input field
Given Click firstName.signUp.Input element
When Enter Emma first name in firstName.signUp.Input field
Given Click lastName.signUp.Input element
When Enter Emma last name in lastName.signUp.Input field
Given Click emailAddress.signUp.Input element
When Enter Emma email in emailAddress.signUp.Input field
Given Click age.signUp.Input element
When Enter Emma age in age.signUp.Input field
Given Click gender.signUp.select element
And Click gender.female.signUp.select.option element
Given Scroll to signUp.Button
Then Click signUp.Button element
Then Assert signUp.VerificationEmailSentText contains signUp.VerificationEmailSent