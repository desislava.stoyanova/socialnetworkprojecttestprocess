Meta:admin

Narrative:
As a system administrator
Admin wants to log in the app and go to Admin page
So that he can delete Peter's post

Scenario: Delete post
Given Admin scrolls to login.Form
And Clicks username.Login.Input element
When Admin types adminUsername in username.Login.Input field
When Clicks password.Login.Input element
When Admin types adminPassword in password.Login.Input field
When Click login.Button element
Then Asserts admin.Button is present
When Admin clicks on admin.Button
And Waits until users.Table is visible
When Confirm element admin.Delete.Button with username Peter is visible
And Admin clicks on admin.Edit.User.Profile
And Admin clicks on admin.Edit.Posts
Then Verify element admin.Delete.Post.Text with text post.Creation.Text is presented
When Admin clicks twice on admin.Delete.Post.Button
Then Verify element admin.Delete.Post.Text with text post.Creation.Text is not presented


