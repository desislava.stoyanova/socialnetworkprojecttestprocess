Meta:admin

Narrative:
As a system administrator
Admin wants to log in the app, go to Admin page and search for Emma's profile
So that he can delete it

Scenario: Delete user's profile after search
Given Admin scrolls to login.Form
And Clicks username.Login.Input element
When Admin types adminUsername in username.Login.Input field
When Clicks password.Login.Input element
When Admin types adminPassword in password.Login.Input field
When Click login.Button element
Then Asserts admin.Button is present
When Admin clicks on admin.Button
And Waits until users.Table is visible
And Clicks search.Field element
And Enter Emma username in search.Field field
When Confirm element admin.Delete.Button with username Emma is visible
When Admin clicks element admin.Delete.Button for username Emma
When Admin clears search.Field
When Admin Types existing user: Emma in search.Field field
Then Assert element admin.Delete.Button with username Emma is not visible









